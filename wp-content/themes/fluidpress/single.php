<?php get_header(); ?>

<!-- SLIDER -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- SLIDER -->

<!-- PAGE TITLE -->
<?php include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->


<section class="two-column-with-sidebar container--large">
<div class="two-column-with-sidebar__container news-content">

    <?php if (have_posts()) { ?>
            <div class="container--large news-content__single-column">    
            <?php while (have_posts()) { ?>
                <?php the_post(); ?>


                                <h3 class="news-content__heading"><?php the_field('blog_heading'); ?></h3>

                                <div class="news-content__left">
                                    <p><?php the_field('blog_content_1'); ?></p>
                                </div>

                                <div class="news-content__right">
                                    <?php $blogbackgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
                                    <?php if (!empty($blogbackgroundImg)) { ?>
                                        <img class="news-banner__img" src="<?php echo $blogbackgroundImg[0]; ?>" alt="<?php the_title(); ?>">
                                    <?php  } else {  ?>
                                        <img class="news-banner__img" src="/wp-content/uploads/2022/04/post-placeholder.png" alt="Image Coming Soon">
                                    <?php }?>
                                </div>

                                <div class="news-content__bottom">
                                    <?php the_field('blog_content_2'); ?>
                                </div>



    

            <?php } ?>
            </div>
    <?php } else { ?>
        <div class="news-content__none container--large">
    
            <p><i class="fas fa-exclamation-circle"></i> There is currently no company news available</p>
        
        </div>
    <?php } ?>

</div>
<?php //get_sidebar(); ?>
<!-- BLOG SIDEBAR -->
<?php include(locate_template('./template-parts/blog-sidebar.php')); ?>
<!-- BLOG SIDEBAR -->
</section>



<?php get_footer(); ?>