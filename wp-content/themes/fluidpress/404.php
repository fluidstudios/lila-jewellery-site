<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fluidpress
 */

get_header();
?>

<section class="slider" data-sizes="50vw">
    <div class="slider__content slider__content-300 text-center">
        <h1 class="slider__title">404</h1>
        <p class="slider__description">That page cannot be found</p>
    </div>
    <?php $banner_image = get_field('banner_image'); ?>

    <img class="slider__img slider__img-300" alt="Page Not Found" src="/wp-content/uploads/2022/03/slider-2.jpg"> 
</section>   




<section class="generic-grey-heading grey-background text-center">
    <div class="container generic-grey-heading__container">
        <h2 class="heading-black">It looks like nothing was found at this location</h2>
    </div>
</section>




<section class="page-not-found">
    <div class="container text-center">

        <h3 class="page-not-found__content">Maybe try one of the links below?</h3>


        <!-- <form role="search" method="get" class="sidebar__form search-field" action="<?php get_site_url() ?>">
            <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
            <input type="search" id="woocommerce-product-search-field-0" class="sidebar__search-field" placeholder="Search…" value="" name="s">
            <input type="hidden" name="post_type" value="products">
        </form> -->

        <?php //echo do_shortcode('[ivory-search id="7804" title="Default Search Form"]'); ?>

        
        
       
        <br>
        <?php fluidpress_header_menu(); ?>
 

    </div>
</section>





<?php
get_footer();
