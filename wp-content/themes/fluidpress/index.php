<?php get_header(); ?>

<!-- SLIDER -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- SLIDER -->

<!-- PAGE TITLE -->
<?php include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->

<section class="two-column-with-sidebar container--large">
<div class="two-column-with-sidebar__container news-content">
    
        <?php if (have_posts()) { ?>
            <div class="container--large news-content__column">    
            <?php while (have_posts()) { ?>
                <?php the_post(); ?>
                <?php $blogbackgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>

                <div class="news-content__col">
                    <div class="news-content__panel">
                    <div class="news-content__thumb">
                        <?php if(!empty($blogbackgroundImg[0])){ ?>
                            <a class="news-content__img-link" href="<?php echo the_permalink(); ?>"><img class="news-content__img" src="<?php echo $blogbackgroundImg[0]; ?>" alt="<?php the_title(); ?>"></a>
                        <?php } else { ?>
                            <a class="news-content__img-link" href="<?php echo the_permalink(); ?>"><img class="news-content__img" src="/wp-content/uploads/woocommerce-placeholder-450x450.png" alt="<?php the_title(); ?>"></a>
                        <?php } ?>
                    </div>
                        <div class="news-content__content">
                            <a href="<?php the_permalink(); ?>" class="news-content__title">
                                <?php 
                                    $title = get_the_title(); 
                                    if (strlen($title) > 60) {
                                        $title = substr($title, 0, 60) . '...';
                                     }
                                     echo $title;
                                ?>
                            </a>
                            <p><?php the_field('blog_excerpt'); ?></p>
                            <?php //the_excerpt(); ?>
                        </div>
                    </div>

                </div>

            <?php } ?>
            </div>
        <?php } else { ?>
            <div class="news-content__none container--large">
       
                <p><i class="fas fa-exclamation-circle"></i> No results</p>
         
            </div>
        <?php } ?>

    
</div>
<?php //get_sidebar(); ?>
<!-- BLOG SIDEBAR -->
<?php include(locate_template('./template-parts/blog-sidebar.php')); ?>
<!-- BLOG SIDEBAR -->




</section>




<?php get_footer(); ?>