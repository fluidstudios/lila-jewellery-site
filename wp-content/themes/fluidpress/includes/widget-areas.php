<?php

/**
 * Register widget areas.
 *
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

function fluidpress_widget_areas()
{
    // Register sidebar widget.
    register_sidebar(
        array(
            'id'            => 'sidebar',
            'name'          => __('Sidebar Widgets', 'fluidpress'),
            'description'   => __('Drag widgets here for them to appear in the sidebar.', 'fluidpress'),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h6>',
            'after_title'   => '</h6>',
        )
    );
}

add_action('widgets_init', 'fluidpress_widget_areas');
