<?php

/**
 * Logo.
 */

$logo = (object) [
    "url" => wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full')[0],
    "alt" => get_bloginfo("name")
];
