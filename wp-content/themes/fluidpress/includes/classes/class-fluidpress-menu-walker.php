<?php

/**
 * Header menu walker.
 *
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

class FluidPress_Header_Menu_Walker extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent  = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"menu-list\">\n";
    }
}

/**
 * Footer menu walker.
 *
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

class FluidPress_Footer_Menu_Walker extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent  = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"menu-list\">\n";
    }
}
