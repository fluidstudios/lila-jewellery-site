<?php

require_once('classes/class-fluidpress-menu-walker.php');

/**
 * Register Menu.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

register_nav_menus(
    array(
        'header_menu'  => esc_html__('Header Menu', 'fluidpress'),
        'top_menu'  => esc_html__('top Menu', 'fluidpress'),
        'login_menu'  => esc_html__('login Menu', 'fluidpress'),
        'logout_menu'  => esc_html__('logout Menu', 'fluidpress'),
        'footer_menu'  => esc_html__('Footer Menu', 'fluidpress'),
        'footer_menu_2'  => esc_html__('Footer Menu 2', 'fluidpress'),
        'footer_menu_3'  => esc_html__('Footer Menu 3', 'fluidpress'),
        'show_cart_menu'  => esc_html__('Show Cart', 'fluidpress')
    )
);

/**
 * Menu.
 *
 * @link http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */

function fluidpress_header_menu()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'header_menu',
            'depth'          => 2,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Header_Menu_Walker(),
        )
    );
}

function fluidpress_top_menu()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'header__top-menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'top_menu',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}

function fluidpress_login_menu()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'header__login-menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'login_menu',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}

function fluidpress_logout_menu()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'header__login-menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'logout_menu',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}

function fluidpress_footer_menu()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'footer__menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'footer_menu',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}

function fluidpress_footer_menu_2()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'footer__menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'footer_menu_2',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}

function fluidpress_footer_menu_3()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'footer__menu',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'footer_menu_3',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}

function fluidpress_cart_menu()
{
    wp_nav_menu(
        array(
            'container'      => false,
            'menu_class'     => 'header__cart',
            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'theme_location' => 'show_cart_menu',
            'depth'          => 1,
            'fallback_cb'    => false,
            'walker'         => new FluidPress_Footer_Menu_Walker(),
        )
    );
}
