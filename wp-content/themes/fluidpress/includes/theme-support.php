<?php

/**
 * Register theme support for languages, menus, post-thumbnails, post-formats etc.
 *
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

function fluidpress_theme_support()
{
    // Switch default core markup for search form, comment form, and comments to output valid HTML5.
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        )
    );

    // Custom logo.
    add_theme_support('custom-logo');

    // Let WordPress manage the document title.
    add_theme_support('title-tag');

    // Add post thumbnail support: http://codex.wordpress.org/Post_Thumbnails
    add_theme_support('post-thumbnails');

    // RSS.
    add_theme_support('automatic-feed-links');

    // Add post formats support: http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

    // Additional theme support for woocommerce 3.0.+.
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}

add_action('after_setup_theme', 'fluidpress_theme_support');
