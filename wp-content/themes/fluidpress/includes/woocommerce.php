<?php
// Add product SKU to shop/category page
add_action( 'woocommerce_shop_loop_item_title', 'ah_designs_show_sku', 5 );
function ah_designs_show_sku(){
    global $product;
    echo '<p class="shop__sku">' . $product->get_sku() . '</p>';
}
// Add product SKU to shop/category page

// move price on shop/category page
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 50 );
// move price on shop/category page

// Add container div to product list description
add_action( 'woocommerce_shop_loop_item_title', 'shop_opening_div', 1 );
function shop_opening_div() {
    $opening_div = '<div class="products__content">';
    echo $opening_div;
}
add_action( 'woocommerce_after_shop_loop_item', 'shop_closing_div', 5 );
function shop_closing_div() {
    $closing_div = '</div>';
    echo $closing_div;
}
// Add container div to product list description

// Remove Sales Flash on shop page
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
// Remove Sales Flash on shop page


// Display description on shop page
add_action( 'woocommerce_after_shop_loop_item_title', 'ah_ins_woocommerce_product_excerpt', 35, 2 );
function ah_ins_woocommerce_product_excerpt() {
     $excerpt = get_the_excerpt();

     if (strlen($excerpt) > 60) {
        $excerpt = substr($excerpt, 0, 60) . '...';
     }

    echo '<p class="shop__description">' . $excerpt . '</p>';

}
// Display description on shop page

// Limit character length of product title
add_filter( 'the_title', 'shorten_woo_product_title', 10, 2 );
function shorten_woo_product_title( $title, $id ) {
    if ( ! is_singular( array( 'product' ) ) && get_post_type( $id ) === 'product' && strlen( $title ) > 30 ) {
        return substr( $title, 0, 37) . '…'; // change last number to the number of characters you want
    } else {
        return $title;
    }
}
// Limit character length of product title

// Create Minimum order requirement for checkout
add_action( 'woocommerce_check_cart_items', 'required_min_cart_subtotal_amount' );
function required_min_cart_subtotal_amount() {

    // HERE Set minimum cart total amount
    $minimum_amount = 180;

    // Total (before taxes and shipping charges)
    $cart_subtotal = WC()->cart->get_subtotal();

    // Add an error notice is cart total is less than the minimum required
    if( $cart_subtotal < $minimum_amount  ) {
        // Display an error message
        wc_add_notice( '<strong>' . sprintf( __("A minimum total purchase amount of %s is required to checkout."), wc_price($minimum_amount) ) . '</strong>', 'error' );
    }
}
// Create Minimum order requirement for checkout

// Hide price from logged out users
add_filter( 'woocommerce_get_price_html', 'ah_hide_price_addcart_not_logged_in', 9999, 2 );

function ah_hide_price_addcart_not_logged_in( $price, $product ) {
   if ( ! is_user_logged_in() ) {
      $price = '<a class="single-product__signin" href="' . get_permalink( wc_get_page_id( 'myaccount' ) ) . '">' . __( 'Sign in to view price', 'fluidpress' ) . '</a>';
      remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
      remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
   }
   return $price;
}
// Hide price from logged out users

// Remove product meta categories and sku from product page
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
// Remove product meta categories and sku from product page

// Remove breadcrumbs from shop/category page
function woocommerce_remove_breadcrumb(){
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
}
add_action( 'woocommerce_before_main_content', 'woocommerce_remove_breadcrumb' );
// Remove breadcrumbs from shop/category page

// Add product SKU to product page
add_action( 'woocommerce_single_product_summary', 'product_sku', 1 );
function product_sku() {
    global $product;
    echo '<p class="single-product__sku">' . $product->get_sku() . '</p>';
}
// Add product SKU to product page

// Remove description heading from product tabs
add_filter( 'woocommerce_product_description_heading', '__return_null' );
// Remove description heading from product tabs

// Add a wrapper div to product detail page
add_action( 'woocommerce_before_single_product_summary', 'add_opening_product_image_container_div', 10 );
function add_opening_product_image_container_div() {
    $opening_div = '<div class="single-product__content-wrapper">';
    echo $opening_div;
}
add_action( 'woocommerce_after_single_product_summary', 'add_closing_product_image_container_div', 1 );
function add_closing_product_image_container_div() {
    $closing_div = '</div>';
    echo $closing_div;
}
// Add a wrapper div to product detail page

// Add order statement to checkout page
add_action( 'woocommerce_after_checkout_form', 'ah_add_order_statement', 1 );
function ah_add_order_statement() {
    $statement_heading = get_field('checkout_title', 22);
    $statement_content = get_field('checkout_content', 22);
    echo '<span class="delivery-policy__statement">';
    echo '<i class="fa-solid fa-star delivery-policy__icon"></i>';
    echo '<div class="card delivery-policy__card">';
    echo '<h5 class="delivery-policy__heading">' . $statement_heading . '</h5>';
    echo $statement_content;
    echo "</div>";
    echo "</span>";
}
// Add order statement to checkout page

// Change excerpt length
function ah_excerpt_length($length){
    return 25;
}
add_filter('excerpt_length', 'ah_excerpt_length');
// Change excerpt length

// Change excerpt length dots
function new_excerpt_more( $more ) {
	return '.....';
}
add_filter('excerpt_more', 'new_excerpt_more');
// Change excerpt length dots

// PRODUCT PAGE CATEGORY SELECT
add_action( 'woocommerce_after_single_product', 'product_page_category_select', 5 );

function product_page_category_select() {
    global $product;
    $stock_status = strip_tags(trim(wc_get_stock_html( $product )));
    if ( $stock_status == "Discontinued" ) {

        $category_select = get_field('category_select');
        if(!empty(get_field('category_select'))){
            //print_r($category_select);
            echo "<style>.related{display:none;}</style>";?>
            <?php if( $term = get_term_by( 'id', $category_select, 'product_cat' ) ){ ?>

                <div class="related-alt">
                            <h2>Sorry we don't sell that particular product anymore..</h2>
                            <p>However we have some very similar products, please see below:</p>
                            <?php $value = $term->slug; ?>
                            <?php $short = '[products limit="4" columns="4" category="'.$value.'" cat_operator="AND"]'; ?>

                                <?php echo do_shortcode($short); ?>

                            <a class="related-alt__link" href="/product-category/<?php echo $value; ?>/">View all products in this category</a>
                </div>
            <?php } ?>
           <?php }
    }


}
// PRODUCT PAGE CATEGORY SELECT

// CHANGE WOO ERROR MESSAGE
add_filter( 'woocommerce_registration_error_email_exists', function() {
    return 'An account is already registered with your email address. <a href="/my-account/" class="showlogin">Please log in.</a>';
} );
// CHANGE WOO ERROR MESSAGE



// SORT SHOP PRODUCTS BY SKU

function sv_add_sku_sorting( $args ) {

	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );

	if ( 'sku' == $orderby_value ) {
		$args['orderby'] = 'meta_value';
		$args['order'] = 'asc'; // lists SKUs alphabetically 0-9, a-z; change to desc for reverse alphabetical
		$args['meta_key'] = '_sku';
	}

	return $args;
}
add_filter( 'woocommerce_get_catalog_ordering_args', 'sv_add_sku_sorting' );


/**
 * Add the option to the orderby dropdown.
 *
 * @param array $sortby the sortby options
 * @return array updated sortby
 */
function sv_sku_sorting_orderby( $sortby ) {

	// Change text above as desired; this shows in the sorting dropdown
	$sortby['sku'] = __( 'Sort by SKU', 'textdomain' );

	return $sortby;
}
add_filter( 'woocommerce_catalog_orderby', 'sv_sku_sorting_orderby' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'sv_sku_sorting_orderby' );

// SORT SHOP PRODUCTS BY SKU

// ADD CUSTOM CLASS TO SHOP LIST ITEM

// add_filter( 'product_cat_class', 'filter_product_cat_class', 10, 3 );
// function filter_product_cat_class( $classes, $class, $category ){
//     if( is_shop() )
//         $classes[] = 'custom_cat';

//     return $classes;
// }

// ADD CUSTOM CLASS TO SHOP LIST ITEM


// FORCE MINIMUM QUANTITY OF 2 ON CHECKOUT

// On single product pages
add_filter( 'woocommerce_quantity_input_args', 'min_qty_input_args', 20, 2 );
function min_qty_input_args( $args, $product ) {

    $quantity = 2;
    $args['min_value'] = $quantity;

    return $args;
}

// On archives pages
add_filter( 'woocommerce_loop_add_to_cart_link', 'min_qty_loop_add_to_cart_button', 50, 2 );
function min_qty_loop_add_to_cart_button( $button, $product  ) {

    $quantity = 2;

    $class = implode( ' ', array_filter( array(
        'button',
        'product_type_' . $product->get_type(),
        $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
        $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
    ) ) );

    $button = sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
        esc_url( $product->add_to_cart_url() ),
        esc_attr( isset( $quantity ) ? $quantity : 1 ),
        esc_attr( $product->get_id() ),
        esc_attr( $product->get_sku() ),
        esc_attr( isset( $class ) ? $class : 'button' ),
        esc_html( $product->add_to_cart_text() )
    );

    return $button;
}

// FORCE MINIMUM QUANTITY OF 2 ON CHECKOUT


add_action( 'woocommerce_after_shop_loop_item', 'ah_show_stock_shop', 10 );

function ah_show_stock_shop() {
   global $product;
   echo wc_get_stock_html( $product );
}



// Display Stock Quantity/Status
add_action( 'woocommerce_after_shop_loop_item', 'bbloomer_show_stock_shop', 10 );

function bbloomer_show_stock_shop() {
   global $product;
   echo wc_get_stock_html( $product );
}
//Display Stock Quantity/Status




// Custom out of stock message

add_action( 'woocommerce_single_product_summary', 'ah_show_stock_message', 30 );

function ah_show_stock_message() {
    $message = get_field('discontinued_products_message', 'options');
   global $product;

        if ( !$product->is_in_stock() ) { ?>
    <div class="discontinued-products">
		     <p class="out-of-stock"><strong> NO LONGER AVAILABLE </strong> </p>
            <p class="out-of-stock__message"> <?php echo $message; ?> </p>
     </div>
        <?php }

}
// Custom out of stock message


// Add VAT To order confirmation

// add_filter( 'woocommerce_get_order_item_totals', 'insert_custom_line_order_item_totals', 10, 3 );
// function insert_custom_line_order_item_totals( $total_rows, $order, $tax_display ){

//     if( ! is_wc_endpoint_url() ) {


//         $total_rows['order_total']['value'] = strip_tags( wc_price( $order->get_total() ) );


//         $new_row = array( 'order_tax_total' => array(
//             'label' => __('VAT:','woocommerce'),
//             'value' => strip_tags( wc_price( $order->get_total_tax() ) )
//         ) );



//         $total_rows += $new_row;
//     }

//     return $total_rows;
// }

// Add VAT To order confirmation


//Add SKU to customer emails

function bbloomer_return_sku( $product ) {
    $sku = $product->get_sku();
    if ( ! empty( $sku ) ) {
       return '<p>SKU: ' . $sku . '</p>';
    } else {
       return '';
    }
 }


 add_filter( 'woocommerce_cart_item_name', 'bbloomer_sku_cart_checkout_pages', 9999, 3 );

 function bbloomer_sku_cart_checkout_pages( $item_name, $cart_item, $cart_item_key  ) {
    $product = $cart_item['data'];
    $item_name .= bbloomer_return_sku( $product );
    return $item_name;
 }


 add_action( 'woocommerce_order_item_meta_start', 'bbloomer_sku_thankyou_order_email_pages', 9999, 4 );

 function bbloomer_sku_thankyou_order_email_pages( $item_id, $item, $order, $plain_text ) {
    $product = $item->get_product();
    echo bbloomer_return_sku( $product );
 }

 /**
 * Remove password strength check.
 */
 function iconic_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
 }
 add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );
//Add SKU to customer emails
