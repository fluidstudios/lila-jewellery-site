<?php
add_action( 'woocommerce_register_form_start', 'ah_business_type' );

function ah_business_type() {


	?>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide login__form-row">
        <label for="registration_field_3"><?php esc_html_e( 'Business Type', 'crf' ) ?><span class="required">*</span></label>

        <select class="business-select" name="registration_field_3" id="registration_field_3">
            <option value="null">-- Please Select --</option>
            <option value="Bricks and Mortar" <?php if($_POST['registration_field_3'] == "Bricks and Mortar"){ echo "selected"; } ?>>Bricks and Mortar</option>
            <option value="Eccommerce" <?php if($_POST['registration_field_3'] == "Eccommerce"){ echo "selected"; } ?>>Eccommerce</option>
        </select>

        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide login__form-row">
            <label for="registration_field_2"><?php esc_html_e( 'Tell us more about your business', 'crf' ) ?><span class="required">*</span></label>
            <textarea id="registration_field_2" name="registration_field_2" class="woocommerce-Input woocommerce-Input--text input-textarea" rows="4" cols="50"><?php if ( ! empty( $_POST['registration_field_2'] ) ) { echo $_POST['registration_field_2']; }  ?></textarea>
        </p>

        <p class="form-row">
        <label for="billing_company"><?php _e( 'Company name', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_company" id="billing_company" value="<?php if ( ! empty( $_POST['billing_company'] ) ) esc_attr_e( $_POST['billing_company'] ); ?>" />
        </p>

        <p class="form-row form-row-first">
        <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
        </p>

        <p class="form-row form-row-last">
        <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
        </p>

        <p class="form-row">
        <label for="reg_billing_address_1"><?php _e( 'Address line 1', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_address_1" id="reg_billing_address_1" value="<?php if ( ! empty( $_POST['billing_address_1'] ) ) esc_attr_e( $_POST['billing_address_1'] ); ?>" />
        </p>

        <p class="form-row">
        <label for="reg_billing_address_2"><?php _e( 'Address line 2', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_address_2" id="reg_billing_address_2" value="<?php if ( ! empty( $_POST['billing_address_2'] ) ) esc_attr_e( $_POST['billing_address_2'] ); ?>" />
        </p>

        <p class="form-row form-row-first">
        <label for="reg_billing_city"><?php _e( 'City', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_city" id="reg_billing_city" value="<?php if ( ! empty( $_POST['billing_city'] ) ) esc_attr_e( $_POST['billing_city'] ); ?>" />
        </p>

        <p class="form-row form-row-last">
        <label for="reg_billing_postcode"><?php _e( 'Postcode', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_postcode" id="reg_billing_postcode" value="<?php if ( ! empty( $_POST['billing_postcode'] ) ) esc_attr_e( $_POST['billing_postcode'] ); ?>" />
        </p>

        <p class="form-row">
        <label for="reg_billing_phone"><?php _e( 'Telephone', 'woocommerce' ); ?><span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_phone" id="reg_billing_phone" value="<?php if ( ! empty( $_POST['billing_phone'] ) ) esc_attr_e( $_POST['billing_phone'] ); ?>" />
        </p>

	<?php
}



function custom_woocommerce_process_registration_errors( $validation_errors, $username, $password, $email ){

    if ( $_POST['registration_field_3'] == "null" ) {
        $validation_errors->add( 'registration_field_3_error', __( 'Business type is required', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
        $validation_errors->add( 'billing_company_error', __( 'Company name is required', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $validation_errors->add( 'billing_first_name_error', __( 'First name is required', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
            $validation_errors->add( 'billing_last_name_error', __( 'Last name is required.', 'woocommerce' ) );
    }
    if ( isset( $_POST['registration_field_2'] ) && empty( $_POST['registration_field_2'] ) ) {
        $validation_errors->add( 'registration_field_2_error', __( 'Company details are required.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
        $validation_errors->add( 'billing_address_1_error', __( 'Address Line 1 is required.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_address_2'] ) && empty( $_POST['billing_address_2'] ) ) {
        $validation_errors->add( 'billing_address_2_error', __( 'Address Line 2 is required.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
        $validation_errors->add( 'billing_city_error', __( 'City name is required.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
        $validation_errors->add( 'billing_postcode_error', __( 'Postcode is required.', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
        $validation_errors->add( 'billing_phone_error', __( 'Telephone number is required.', 'woocommerce' ) );
    }

    if ( !isset( $_POST['email'] ) || $_POST['email'] == '' ) {
        $validation_errors->add( 'email', __( 'Please enter email address.', 'woocommerce' ) );
    }

    if ( !isset( $_POST['password'] ) || $_POST['password'] == '' ) {
        $validation_errors->add( 'password', __( 'Please enter password.', 'woocommerce' ) );
    }
    
    return $validation_errors;
}

add_filter( 'woocommerce_process_registration_errors', 'custom_woocommerce_process_registration_errors', 10, 4 );



// add_action( 'woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3 );

// function wooc_validate_extra_register_fields( $username, $email, $validation_errors ) {
//     if ( $_POST['business_type'] == 0 ) {
//         $validation_errors->add( 'business_type_error', __( '<strong>Error</strong>: Business type is required!', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
//         $validation_errors->add( 'billing_company_error', __( '<strong>Error</strong>: Company name is required!', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
//         $validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
//             $validation_errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['business_detail'] ) && empty( $_POST['business_detail'] ) ) {
//         $validation_errors->add( 'business_detail_error', __( '<strong>Error</strong>: Company details are required!.', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
//         $validation_errors->add( 'billing_address_1_error', __( '<strong>Error</strong>: Address Line 1 is required!.', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_address_2'] ) && empty( $_POST['billing_address_2'] ) ) {
//         $validation_errors->add( 'billing_address_2_error', __( '<strong>Error</strong>: Address Line 2 is required!.', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
//         $validation_errors->add( 'billing_city_error', __( '<strong>Error</strong>: City name is required!.', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
//         $validation_errors->add( 'billing_postcode_error', __( '<strong>Error</strong>: Postcode is required!.', 'woocommerce' ) );
//     }
//     if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
//         $validation_errors->add( 'billing_phone_error', __( '<strong>Error</strong>: Telephone number is required!.', 'woocommerce' ) );
//     }

//        return $validation_errors;
// }



add_action( 'user_register', 'crf_user_register' );

function crf_user_register( $user_id ) {
	if ( ! empty( $_POST['registration_field_3'] ) ) {
		update_user_meta( $user_id, 'registration_field_3', $_POST['registration_field_3'] );
	}
    if ( ! empty( $_POST['registration_field_2'] ) ) {
		update_user_meta( $user_id, 'registration_field_2', $_POST['registration_field_2']  );
        update_user_meta( $user_id, 'description', $_POST['registration_field_2']  );
	}
    if ( ! empty( $_POST['billing_company'] ) ) {
		update_user_meta( $user_id, 'billing_company', $_POST['billing_company']  );
	}
    if ( ! empty( $_POST['billing_first_name'] ) ) {
		update_user_meta( $user_id, 'billing_first_name', $_POST['billing_first_name']  );
        update_user_meta( $user_id, 'shipping_first_name', $_POST['billing_first_name']  );
        update_user_meta( $user_id, 'first_name', $_POST['billing_first_name']  );
	}
    if ( ! empty( $_POST['billing_last_name'] ) ) {
		update_user_meta( $user_id, 'billing_last_name', $_POST['billing_last_name']  );
        update_user_meta( $user_id, 'shipping_last_name', $_POST['billing_last_name']  );
        update_user_meta( $user_id, 'last_name', $_POST['billing_last_name']  );
	}
    if ( ! empty( $_POST['billing_address_1'] ) ) {
		update_user_meta( $user_id, 'billing_address_1', $_POST['billing_address_1']  );
	}
    if ( ! empty( $_POST['billing_address_2'] ) ) {
		update_user_meta( $user_id, 'billing_address_2', $_POST['billing_address_2']  );
	}
    if ( ! empty( $_POST['billing_city'] ) ) {
		update_user_meta( $user_id, 'billing_city', $_POST['billing_city']  );
	}
    if ( ! empty( $_POST['billing_postcode'] ) ) {
		update_user_meta( $user_id, 'billing_postcode', $_POST['billing_postcode']  );
	}
    if ( ! empty( $_POST['billing_phone'] ) ) {
		update_user_meta( $user_id, 'billing_phone', $_POST['billing_phone']  );
	}
    
}



add_action( 'show_user_profile', 'crf_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'crf_show_extra_profile_fields' );

function crf_show_extra_profile_fields( $user ) {
	?>
	<h3><?php esc_html_e( 'Personal Information', 'crf' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="registration_field_3"><?php esc_html_e( 'Business Type', 'crf' ); ?></label></th>
			<td>
                <?php 
                $ah_value = esc_html( get_the_author_meta( 'registration_field_3', $user->ID ) ); 
                echo $ah_value;
                
                // if($ah_value == 1) {
                //     echo "Online Seller";
                // } elseif($ah_value == 2){
                //     echo "Bricks &amp; Morter";
                // } else {
                //     echo "";
                // }
                ?>
            </td>
		</tr>
        <tr>
            <th><label for="registration_field_2"><?php esc_html_e( 'Business Details', 'crf' ); ?></label></th>
            <td>
                <?php echo esc_html( get_the_author_meta( 'registration_field_2', $user->ID ) ) ?>
            </td>
        </tr>
	</table>
	<?php
}