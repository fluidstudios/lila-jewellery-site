<?php

/**
 * Load stylesheets.
 */

function load_stylesheets()
{
        // Enqueue Font Awesome
        wp_register_style('font-awesome-free', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css');
        wp_enqueue_style('font-awesome-free');
        
        // Enqueue Slick Slider stylesheet.
        wp_register_style('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', [], null, 'all');
        wp_enqueue_style('slick');


        // Enqueue our main stylesheet.
        wp_register_style('main', get_template_directory_uri() . '/dist/stylesheets/main.min.css', [], null, 'all');
        wp_enqueue_style('main');

}

add_action('wp_enqueue_scripts', 'load_stylesheets');

/**
 * Load scripts.
 */

function load_scripts()
{
        // Deregister the default WordPress version of jQuery and enqueue our own.
        // wp_deregister_script('jquery');
        // wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js?ver=5.8.1', [], false, true);
        // wp_enqueue_script('jquery');

        // Slick Slider Javascript
        wp_register_script('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', [], false, true);
        wp_enqueue_script('slick');

        // Deregister the default WordPress version of jQuery Migrate and enqueue our own.
        // wp_deregister_script('jquery-migrate');
        // wp_register_script('jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.2/jquery-migrate.min.js', 'jquery', false, true);
        // wp_enqueue_script('jquery-migrate');

        // Enqueue our bundled JavaScript.
        wp_register_script('bundle', get_template_directory_uri() . '/dist/scripts/bundle.min.js', [], false, true);
        wp_enqueue_script('bundle');
}

add_action('wp_enqueue_scripts', 'load_scripts');

/**
 * Load admin script.
 */

function load_admin_scripts() {
        wp_register_script('notes-text', get_template_directory_uri() . '/src/scripts/notes.js', [], false, true);
        wp_enqueue_script('notes-text');
    }
    
    add_action('admin_enqueue_scripts', 'load_admin_scripts');