<?php

/**
 * Configure responsive images sizes
 *
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

// Add featured image sizes.
add_image_size('small', 640, 200, true);
add_image_size('medium', 1280, 400, true);
add_image_size('large', 1440, 400, true);
add_image_size('full', 1920, 400, true);

// Remove inline width and height attributes for post thumbnails.
function remove_thumbnail_dimensions($html, $post_id, $post_image_id)
{
    if (!strpos($html, 'attachment-shop_single')) {
        $html = preg_replace('/^(width|height)=\"\d*\"\s/', '', $html);
    }

    return $html;
}

add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3);
