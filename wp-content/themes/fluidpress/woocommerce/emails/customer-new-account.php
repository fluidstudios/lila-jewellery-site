<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 6.0.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer username */ ?>
<p><?php printf( esc_html__( 'Dear %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
<p><?php printf( esc_html__( 'Your account is waiting for approval. Once approved, you will receive an email and then you will be able to see trade prices and place your first order. A carriage paid order is £180.00.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( 'The most important thing to remember is you are in control of the order process throughout; you don’t have to pay until you are 100% ready and happy to go ahead. Here’s how it works:', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( '1. Place your order via our website, email, or telephone. Tell us if you have any special delivery instructions such as a delayed delivery date.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( '2. We will then check your order to make sure it is correct, and we haven’t missed anything, or an item has gone out of stock.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( '3. In the unlikely event there are any issues we will either email or call you.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( '4. Once you are 100% satisfied, we will ask for your payment either by bank transfer or card payment.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( '5. After payment is made, we will dispatch by next day courier within 3 working days or sooner.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( 'If you have any questions about our collections, please contact me on 01582 841158, I will be happy to help.', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( 'Kind Regards', 'woocommerce') ); ?></p>
<p><?php printf( esc_html__( 'Lila Jewellery', 'woocommerce') ); ?></p>


<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated && $set_password_url ) : ?>
	<?php // If the password has not been set by the user during the sign up process, send them a link to set a new password ?>
	<p><a href="<?php echo esc_attr( $set_password_url ); ?>"><?php printf( esc_html__( 'Click here to set your new password.', 'woocommerce' ) ); ?></a></p>
<?php endif; ?>

<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

do_action( 'woocommerce_email_footer', $email );
