<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-end.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$template = wc_get_theme_slug_for_templates();
//$sidebar = include(locate_template('./template-parts/blog-sidebar.php')); 
$sidebar = "xxx";

switch ( $template ) {
	case 'twentyten':
		echo '</div></div>';
		break;
	case 'twentyeleven':
		echo '</div>';
		get_sidebar( 'shop' );
		echo '</div>';
		break;
	case 'twentytwelve':
		echo '</div></div>';
		break;
	case 'twentythirteen':
		echo '</div></div>';
		break;
	case 'twentyfourteen':
		echo '</div></div></div>';
		get_sidebar( 'content' );
		break;
	case 'twentyfifteen':
		echo '</div></div>';
		break;
	case 'twentysixteen':
		echo '</main></div>';
		break;
	default:
		echo '</div>';
		echo '<div class="sidebar">';
		echo '<h6 class="sidebar__heading">Search</h6>';
		echo do_shortcode('[smart_search id="1"]');
		//echo '<form role="search" method="get" class="sidebar__form search-field" action="'.get_site_url().'">';
		//echo '<label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>';
		//echo '<input type="search" id="woocommerce-product-search-field-0" class="sidebar__search-field" placeholder="Search…" value="" name="s">';
		//echo '<input type="hidden" name="post_type" value="products">';
		//echo '</form>';
		echo '<h6 class="sidebar__heading">Design Categories</h6>';
		echo fluidpress_header_menu();
		echo '</div>';
		echo '</div>';
		
		break;
}

