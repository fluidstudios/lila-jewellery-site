<?php
/**
 * Displayed when no products are found matching the current query
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/no-products-found.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.0.0
 */

defined( 'ABSPATH' ) || exit;

?>
<p class="woocommerce-info"><?php esc_html_e( 'Sorry there are no products in this category yet.', 'woocommerce' ); ?></p>
<p><?php 

foreach (get_the_category() as $category) {
    echo $category->name;
    print_r($category->name);
    if ( $category->name === 'end-of-line-clearance' ) {
        echo 'xxxxxxxx'; //Markup as you see fit
    }
}

?></p>
