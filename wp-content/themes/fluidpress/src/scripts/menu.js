// Menu.
jQuery(".toggle").on("click", () => {
  if (jQuery(window).width() <= 1024) {
    jQuery("body").toggleClass("toggle--menu");
    jQuery(".toggle").toggleClass("toggle--open");
    jQuery(".navigation").toggleClass("navigation--open");
    jQuery(".header").toggleClass("header--open");
  } else {
    jQuery(".toggle").removeClass("toggle--open");
    jQuery(".navigation").removeClass("navigation--open");
  }
});
// $(".toggle").on("click", () => {
//   $(".toggle").toggleClass("toggle--open");
//   $(".navigation").slideToggle();
// });