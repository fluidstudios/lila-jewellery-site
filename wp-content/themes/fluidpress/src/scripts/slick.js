jQuery(document).on('ready', function() {

    jQuery(".home-news__slider").slick({
      dots: false,
      arrows: true,
      autoplay: false,
      speed: 1000,
      slidesToShow: 4,
      autoplaySpeed: 2000,
      responsive: [{
        breakpoint: 1490,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1
        }
      },{
        breakpoint: 1250,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1190,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 630,
        settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
        }
      }]
    });
  });