jQuery(document).ready(function(){
    jQuery('.accordion-list > li > .answer').hide();
    jQuery('.accordion-list > #ah-trigger > .answer').show();  
    jQuery('.accordion-list > li').click(function() {
      if (jQuery(this).hasClass("active")) {
        jQuery(this).removeClass("active").find(".answer").slideUp();
      } else {
        jQuery(".accordion-list > li.active .answer").slideUp();
        jQuery(".accordion-list > li.active").removeClass("active");
        jQuery(this).addClass("active").find(".answer").slideDown();
      }
      return false;
    });
    
  });