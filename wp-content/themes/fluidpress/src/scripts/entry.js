/**
 * Entry.
 *
 * Module entry point.
 */

import "./slick";
import "./menu";
import "./faqs";
