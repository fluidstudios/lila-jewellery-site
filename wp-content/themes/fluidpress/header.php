<?php include 'includes/logo.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="<?php echo home_url();  ?>/wp-content/uploads/2022/04/og-image.png" />
    <meta property="og:description" content="As a leading fashion jewellery supplier, we know that quality and style are key and pride ourselves on our excellent products and service." />
    <meta property="og:url"content="<?php echo home_url();  ?>" />
    <meta property="og:title" content="Lila Jewellery | Providers of Wholesale Jewellery" />

    <?php 
    $domain = get_option('siteurl');

    if($domain == "https://lilajewellery.co.uk"){ ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NRK5J49');</script>
    <!-- End Google Tag Manager -->

    <?php } ?>

 

    <?php if ( is_user_logged_in() ) { ?>
        <style>
            .out-of-stock {
                display:block;
            }

        </style>
     <?php } else { ?>
        <style>
            .out-of-stock {
                display:none !important;
            }

        </style>
     <?php } ?>

    <?php wp_head(); ?>
    <style>
        @media (min-width: 1024px) {
            .header__login-menu {
                position: absolute;
            }
        }
    </style>
</head>

<body <?php body_class(); ?>>



<?php if($domain == "https://lilajewellery.co.uk"){ ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NRK5J49"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php } ?>

    <header class="header">

        <section class="header__top">
            <div class="header__container">
                <p class="header__top-content"><?php the_field('header_top_text', 'option'); ?></p>
                <?php fluidpress_top_menu(); ?>
            </div>
        </section>
		<div class="mobileonly">
			<div class="banner"><?php the_field('banner_text', 'option'); ?></div>
		</div>
        <section class="header__layout container">
			<div class="banner desktoponly"><?php the_field('banner_text', 'option'); ?></div>
            <a href="/">
                <img src="<?= $logo->url ?>" alt="<?= $logo->alt ?>" class="header__logo">
            </a>

            <?php if ( is_user_logged_in() ) { ?>
                <?php fluidpress_logout_menu(); ?>
            <?php  } else { ?>
                <?php fluidpress_login_menu(); ?>
            <?php } ?>


            <button class="toggle">
                <span class="toggle__line"></span>
            </button>

        </section>

        <div class="header__navbar">
            <nav class="navigation"><?php fluidpress_header_menu(); ?></nav>
            <?php fluidpress_cart_menu(); ?>
        </div>

    </header>

<?php if( is_product_category() ) { ?>
    <?php include(locate_template('./template-parts/breadcrumbs-category.php')); ?>
<?php } elseif( is_shop() ){ ?>
    <?php include(locate_template('./template-parts/breadcrumbs-shop.php')); ?>
<?php } elseif( is_product() ) { ?>
    <?php include(locate_template('./template-parts/breadcrumbs-product-detail.php')); ?>
<?php } else { ?>
    <?php echo ""; ?>
<?php } ?>    