<?php get_header(); ?>

<!-- SLIDER -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- SLIDER -->

<!-- PAGE TITLE -->
<?php //include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->

<section class="two-column-with-sidebar container--large">
<div class="two-column-with-sidebar__container search-content">
    
        <?php if (have_posts()) { ?>
            <div class="container--large search-content__column">    
            <?php while (have_posts()) { ?>
                <?php the_post(); ?>
                <?php $blogbackgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>

                <div class="search-content__col">
                    <div class="search-content__panel">
                    <div class="search-content__thumb">
                        <a class="search-content__link" href="<?php echo the_permalink(); ?>"><img class="search-content__img" src="<?php echo $blogbackgroundImg[0]; ?>" alt="<?php the_title(); ?>"></a>
                    </div>
                        <div class="search-content__content">
                            <a href="<?php the_permalink(); ?>" class="search-content__title">
                                <?php 
                                    $title = get_the_title(); 
                                    if (strlen($title) > 60) {
                                        $title = substr($title, 0, 60) . '...';
                                     }
                                     echo $title;
                                ?>
                            </a>
                            <?php the_excerpt(); ?>
                        </div>
                    </div>

                </div>

            <?php } ?>
            </div>
        <?php } else { ?>
            <div class="search-content__none container--large">
       
                <p><i class="fas fa-exclamation-circle"></i> There is currently no company news available</p>
         
            </div>
        <?php } ?>

    
</div>
<?php //get_sidebar(); ?>
<div class="sidebar">

    <h6 class="sidebar__heading">Search</h6>

    <?php echo do_shortcode('[ivory-search id="7804" title="Default Search Form"]') ?>

    <!-- <form role="search" method="get" class="sidebar__form search-field" action="<?php //get_site_url() ?>">
        <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
        <input type="search" id="woocommerce-product-search-field-0" class="sidebar__search-field" placeholder="Search…" value="" name="s">
        <input type="hidden" name="post_type" value="products">
    </form> -->

    <h6 class="sidebar__heading">Design Categories</h6>

        <?php echo fluidpress_header_menu(); ?>




  

</div>




</section>




<?php get_footer(); ?>