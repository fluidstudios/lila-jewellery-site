<?php

/**
 * Functions.
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FluidPress
 * @since FluidPress 1.0.0
 */

// Various cleanup functions.
require_once('includes/cleanup.php');

// Enqueue scripts and stylesheets.
require_once('includes/enqueue-scripts.php');

// Register navigation menu.
require_once('includes/navigation.php');

// Create widget areas.
require_once('includes/widget-areas.php');

// Add theme support.
require_once('includes/theme-support.php');

// Configure image sizes.
//require_once('includes/responsive-images.php');

// Woocommerce functions
require_once('includes/woocommerce.php');

// ACF Fields
require_once('includes/acf.php');

// SVG Support
require_once('includes/svg-support.php');

// Custom Registration fields
require_once('includes/custom-registration.php');

// Add Registration Date to users in backend
require_once('includes/registration-date.php');

