<section class="layout design-categories pt-3 pb-3 grey-background"> 
    <h2 class="heading-black text-center"><?php the_field('design_categories_title'); ?></h2>
    <div class="container--large layout__column design-categories__column pt-2">

    <?php if (have_rows('home_categories')) : ?>
    <?php while (have_rows('home_categories')) : the_row(); ?>

        <?php $home_category_img = get_sub_field('home_category_image') ?>

         <a href="<?php the_sub_field('home_category_url'); ?>" class="layout__4-col design-categories__col">
            <span class="design-categories__wrapper">

                <?php if(!empty(get_sub_field('home_category_image'))){ ?>
                    <img class="design-categories__img" alt="<?php echo $home_category_img['alt']; ?>" src="<?php echo $home_category_img['url']; ?>">
                <?php } else { ?>
                    <img class="design-categories__img" alt="Image Coming Soon" src="/wp-content/uploads/2022/04/post-placeholder-2.png">
                <?php } ?>
            
            <h5 class="design-categories__link"><?php the_sub_field('home_category_title'); ?></h5>
            </span>
        </a>

    <?php endwhile; ?>
    <?php endif; ?>

    </div>

</section>

