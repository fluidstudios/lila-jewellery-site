<?php $banner_color = get_field('banner_colour'); ?>
<?php $show_hide = get_field('show_hide_scrolldown'); ?>

<?php $banner_image = get_field('banner_image'); ?>
<?php $banner_image_mobile = get_field('banner_image_mobile'); ?>

<section class="slider slider__img-mobile" style="<?php if(!empty(get_field('banner_image_mobile'))){ echo "background-image: url(".$banner_image_mobile['url']."); background-repeat: no-repeat; background-size: cover; background-position: center;"; } ?>" data-sizes="50vw">

    <div class="slider__content slider__content-600 text-center" style="color:<?php the_field('banner_colour'); ?>;">
        <h1 class="slider__title"><?php the_field('banner_title'); ?></h1>
        <p class="slider__description" style="color:<?php the_field('banner_colour'); ?>"><?php the_field('banner_content'); ?></p>
        <a class="button <?php if(get_field('banner_colour') == "white"){ echo "slider__btn-white"; } else { echo "slider__btn"; } ?>" href="<?php the_field('banner_button_link'); ?>"><?php the_field('banner_button'); ?></a>
    </div>

    <?php if($show_hide == "show"){  ?>
        <div class="slider__scroll text-center">
            <a class="slider__icon-link" href="#scroll-1"><i class="slider__icon fas fa-arrow-alt-circle-down"></i></a>
            <a href="#scroll-1">Scroll down for more info </a>
        </div>
    <?php } else { 
        echo "";
     } ?>



    <?php if(!empty(get_field('banner_image'))){ ?>
        <img class="slider__img slider__img-600" alt="<?php echo $banner_image['alt']; ?>" src="<?php echo $banner_image['url']; ?>" >
    <?php } else { ?>
        <img class="slider__img slider__img-600" alt="Carousel Image Missing" src="https://via.placeholder.com/1500x500.pngtext=Carousel+Image+Missing" >
    <?php } ?>


        

</section>   

    