<section class="breadcrumbs">
    <div class="container breadcrumbs__container">
    <i class="fa-solid fa-house breadcrumbs__home-icon"></i>
        <?php $args = array(
            'delimiter' => '<i class="fa-solid fa-chevron-right breadcrumbs__icon"></i>',
            'before' => '<span class="breadcrumbs__title">' . __( '', 'woothemes' ) . '</span>'
            );
            woocommerce_breadcrumb( $args );
        ?>
    </div>
</section>