<section class="breadcrumbs">

    <div class="container breadcrumbs__container">
    <i class="fa-solid fa-house breadcrumbs__home-icon"></i>

    <?php
    global $product;
        if ( ! is_a( $product, 'WC_Product' ) ) {
            $product = wc_get_product( get_the_id() );
        }
    ?>


        <nav class="woocommerce-breadcrumb"><span class="breadcrumbs__title"></span><a href="/">Home</a><i class="fa-solid fa-chevron-right breadcrumbs__icon"></i><span class="breadcrumbs__title"><a href="/shop/">Shop</a></span><i class="fa-solid fa-chevron-right breadcrumbs__icon"></i><span class="breadcrumbs__title"><?php echo $wp_query->get_queried_object()->name; ?></span></nav>
    </div>

</section>