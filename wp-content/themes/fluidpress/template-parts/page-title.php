<section class="generic-grey-heading grey-background">
    <div class="container generic-grey-heading__container">
        <h1 class="heading-black"><?php the_title(); ?></h1>
    </div>
</section>

