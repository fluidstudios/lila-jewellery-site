<div class="sidebar">

    <h6 class="sidebar__heading">Search</h6>

    <form role="search" method="get" class="sidebar__form search-field" action="<?php echo get_site_url() ?>/our-blog/">
        <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
        <input type="search" id="woocommerce-product-search-field-0" class="sidebar__search-field" placeholder="Search…" value="" name="s">
        <input type="hidden" name="post_type" value="post">
    </form>

    <h6 class="sidebar__heading">Blog Categories</h6>



    <?php
    $args = array(
        'post_status'=>'publish',
        'post_type'=>'post',
        'order' => 'DESC',
        'orderby'   => 'meta_value',
        'posts_per_page'=>-1
    );
    $get_posts = new WP_Query();
    $i = 0;
    $get_posts->query($args);
    if($get_posts->have_posts()) {
        $cats = array();
        while($get_posts->have_posts()) { $get_posts->the_post();
            $post_categories = wp_get_post_categories( get_the_ID() );
            foreach($post_categories as $c){
                $cat = get_category( $c );
                $cats[$i] = $cat->slug ;
                $i++;
            }
        } 
        $results = array_unique($cats);

        echo '<ul class="sidebar__menu">';
        sort($results);
        foreach($results as $result){ ?>
                <?php $result_list = str_replace("-", " ", $result); ?>
                
                <li class="sidebar__menu-list"><a href="/category/<?php echo $result; ?>"><?php echo $result_list; ?></a></li>
       <?php  }
        echo '</ul>';
    } 
    wp_reset_postdata();
?>

</div>