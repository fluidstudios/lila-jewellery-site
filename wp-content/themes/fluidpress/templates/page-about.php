<?php /* Template Name: lila-AboutPage */ ?>
<?php get_header(); ?>

<!-- BREADCRUMBS -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- BREADCRUMBS -->

<!-- SLIDER -->
<?php include(locate_template('./template-parts/slider.php')); ?>
<!-- SLIDER -->

<!-- OUR DESIGN CATEGORIES -->
<?php include(locate_template('./template-parts/our-design-categories.php')); ?>
<!-- OUR DESIGN CATEGORIES -->



<!-- WELCOME TEXT -->
<section class="home-welcome pt-4 pb-4 text-center">
    <div class="container">
        <h2 class="heading-black"><?php the_field('about_us_title'); ?></h2>
        <span class="content-large pl-4 pr-4">
                <?php the_field('about_us_content'); ?>
        </span>
    </div>
</section>
<!-- WELCOME TEXT -->



<?php get_footer(); ?>