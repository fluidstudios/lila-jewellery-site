<?php /* Template Name: lila-TestimonialsPage */ ?>
<?php get_header(); ?>

<!-- SLIDER -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- SLIDER -->


<!-- PAGE TITLE -->
<?php include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->

<section class="generic-content testimonials">
    <div class="container generic-content__container">

            <div class="content-large">
                <p>Here are some testimonials from a small selection of our very satisfied customers, if you would like to speak to them directly then please contact us for further details.</p>
            </div>
            
            <div class="">
                <div class="testimonials__column">



                    <?php if (have_rows('testimonials')) : ?>
                    <?php while (have_rows('testimonials')) : the_row(); ?>

                        <div class="testimonials__3-col">
                            <div class="card">
                                <h4 class="testimonials__heading"><?php the_sub_field('testimonial_title'); ?></h4>
                                <h6 class="testimonials__name"><?php the_sub_field('testimonial_authors_name'); ?></h6>
                                <p><?php the_sub_field('testimonial_content'); ?></p>
                            </div>
                        </div>


                    <?php endwhile; ?>
                    <?php endif; ?>

                    <!-- <div class="testimonials__3-col">
                        <div class="card">
                            <h4 class="testimonials__heading">Garden Centre, North East</h4>
                            <h6 class="testimonials__name">Lesley</h6>
                            <p>My customers love Lila jewellery, the quality is excellent and they love the fact they come packed in lovely gift boxes.</p>
                        </div>
                    </div> -->

                    <!-- <div class="testimonials__3-col">
                        <div class="card">
                            <h4 class="testimonials__heading">Gift Shop, Midlands</h4>
                            <h6 class="testimonials__name">Dorothy</h6>
                            <p>We order every month, there is always some new designs that catch our eye, if they catch our eye then they will catch my customers eye, so it all works very well. Thanks alot.</p>
                        </div>
                    </div>

                    <div class="testimonials__3-col">
                        <div class="card">
                            <h4 class="testimonials__heading">Gift shop, West Country</h4>
                            <h6 class="testimonials__name">Tilly</h6>
                            <p>Excellent and reliable wholesale jewellery supplier. They have an interesting and diverse range so there is always something to offer your customers. Well worth checking their website for new ideas.</p>
                        </div>
                    </div>

                    <div class="testimonials__3-col">
                        <div class="card">
                            <h4 class="testimonials__heading">Jewellery Shop, North East</h4>
                            <h6 class="testimonials__name">Maureen</h6>
                            <p>Lila supplies a large range of jewellery and are always coming up with new ideas and designs at terrific prices, whether its wholesale jewellery or something a little more special I would strongly recommend them as an excellent supplier.</p>
                        </div>
                    </div>

                    <div class="testimonials__3-col">
                        <div class="card">
                            <h4 class="testimonials__heading">Community Fundraiser for the Poppy Appeal in Hertfordshire</h4>
                            <h6 class="testimonials__name">Natasha Robertson</h6>
                            <p>A huge thank you to Frank Ross Ltd for their support. The message behind this year’s Poppy Appeal is Live On – to the memory of the fallen and future of the living. Every donation we receive from Frank Ross Ltd makes a real difference to the lives of Service men and women, veterans and their loved ones.</p>
                        </div>
                    </div> -->

                </div>
            </div>
    </div>
</section>




<?php get_footer(); ?>