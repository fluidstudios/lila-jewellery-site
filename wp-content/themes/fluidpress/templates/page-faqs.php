<?php /* Template Name: lila-FaqsPage */ ?>
<?php get_header(); ?>


<!-- SLIDER -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- SLIDER -->


<!-- PAGE TITLE -->
<?php include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->



<section class="generic-content faqs">
    <div class="container generic-content__container">

            <div class="content-large">
                <p>Please see below for our FAQ section. If you still can’t see the answer to your query please <a href="/contact/">contact us today</a>.</p>
            </div>

            <ul class="accordion-list">
                    <?php $count = 0; ?>
                    <?php if (have_rows('questions')) : ?>
                        <?php while (have_rows('questions')) : the_row(); ?>
                            <?php $count++; ?>

                            <li <?php if ($count == 1) {
                                    echo 'id="ah-trigger" class="active"';
                                } ?>>
                                <h3><?php echo $count; ?>. <?php echo get_sub_field('question'); ?></h3>
                                <div class="answer">
                                    <?php echo get_sub_field('answer'); ?>
                                </div>
                            </li>

                        <?php endwhile; ?>
                    <?php endif; ?>

                </ul>
            

    </div>
</section>



<?php get_footer(); ?>