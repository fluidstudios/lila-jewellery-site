<?php /* Template Name: lila-DeliveryPage */ ?>
<?php get_header(); ?>


<!-- SLIDER -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- SLIDER -->


<!-- PAGE TITLE -->
<?php include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->



<section class="generic-content delivery-policy">
    <div class="container generic-content__container">

            <div class="content-large">
                <p><?php the_field('page_delivery_content_top'); ?></p>
                
                <span class="delivery-policy__statement">
                    <i class="fa-solid fa-star delivery-policy__icon"></i>
                    <div class="card delivery-policy__card">
                        <h5 class="delivery-policy__heading"><?php the_field('wholesale_orders_title'); ?></h5>
                        <?php the_field('wholesale_orders_content'); ?>
                    </div>
                </span>

                <h2 class="heading-small-black"><?php the_field('page_delivery_content_bottom_title'); ?></h2>
                <?php the_field('page_delivery_content_bottom'); ?>
            </div>
            

    </div>
</section>



<?php get_footer(); ?>