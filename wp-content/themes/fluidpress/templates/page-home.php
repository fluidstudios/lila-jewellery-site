<?php /* Template Name: lila-HomePage */ ?>
<?php get_header(); ?>

<!-- SLIDER -->
<?php include(locate_template('./template-parts/slider.php')); ?>
<!-- SLIDER -->

<!-- SALES BANNER -->
<section class="sales-banner grey-background">
    <div class="container sales-banner__column">

            <div class="sales-banner__col">
                <div class="sales-banner__left">
                    <i class="sales-banner__icon fa-solid <?php the_field('sales_banner_icon_1'); ?>"></i>
                </div>
                <div class="sales-banner__right">
                    <h3 class="sales-banner__heading"><?php the_field('sales_banner_title_1'); ?></h3>
                    <p class="sales-banner__content"><?php the_field('sales_banner_content_1'); ?></p>
                </div>
            </div>

            <div class="sales-banner__col">
                <div class="sales-banner__left">
                    <i class="sales-banner__icon fa-solid <?php the_field('sales_banner_icon_2'); ?>"></i>    
                </div>
                <div class="sales-banner__right">
                    <h3 class="sales-banner__heading"><?php the_field('sales_banner_title_2'); ?></h3>
                    <p class="sales-banner__content"><?php the_field('sales_banner_content_2'); ?></p>
                </div>
            </div>

            <div class="sales-banner__col">
                <div class="sales-banner__left">
                    <i class="sales-banner__icon fa-solid <?php the_field('sales_banner_icon_3'); ?>"></i>
                </div>
                <div class="sales-banner__right">
                    <h3 class="sales-banner__heading"><?php the_field('sales_banner_title_3'); ?></h3>
                    <p class="sales-banner__content"><?php the_field('sales_banner_content_3'); ?></p>
                </div>
            </div>

    </div>
    <span id="scroll-1"></span>
</section>
<!-- SALES BANNER -->

<!-- WELCOME TEXT -->
<section class="home-welcome pt-4 pb-4 text-center">
    <div class="container">
        <h3 class="heading-gold"><?php the_field('intro_heading'); ?></h3>
        <h2 class="heading-black"><?php the_field('intro_sub_heading'); ?></h2>
        <span class="content-large pl-4 pr-4">
            <?php the_field('intro_content'); ?>
        </span>
    </div>
</section>
<!-- WELCOME TEXT -->



<!-- OUR DESIGN CATEGORIES -->
<?php include(locate_template('./template-parts/our-design-categories.php')); ?>
<!-- OUR DESIGN CATEGORIES -->


<!-- OUR JEWELLERY -->

<?php $our_jewellery_image = get_field('our_jewellery_image') ?>
<section class="layout our-jewellery pt-4 pb-4">
    <div class="container--large layout__column our-jewellery__column">
        <div class="layout__2-col our-jewellery__2-col">
            <?php if(!empty(get_field('our_jewellery_image'))){ ?>
                    <img class="our-jewellery__img" alt="<?php echo $our_jewellery_image['alt']; ?>" src="<?php echo $our_jewellery_image['url']; ?>">
               <?php } else { ?>
                    <img class="our-jewellery__img" alt="Missing Image" src="/wp-content/uploads/2022/04/post-placeholder.png">
               <?php } ?>
            
        </div>
        <div class="layout__2-col our-jewellery__2-col">
            <h2 class="heading-black"><?php the_field('our_jewellery_title'); ?></h2>
            <span class="content-large">
                <?php the_field('our_jewellery_content'); ?>
                <a class="button our-jewellery__btn" href="<?php the_field('our_jewellery_button_link'); ?>"><?php the_field('our_jewellery_button'); ?></a>
            </span>
            
        </div>
    </div>
</section>
<!-- OUR JEWELLERY -->


<!-- WANT TO MAKE AN ORDER -->
<section class="home-contact grey-background pt-2 pb-2 text-center">
    <div class="container home-contact__container">
            <h2 class="heading-blue"><?php the_field('make_an_order_title'); ?></h2>
            <span class="content-large">
                    <?php the_field('make_an_order_content'); ?>
            </span>
            <div class="contact-form">
                <div class="contact-form__column">
                    <?php $short = get_field('make_an_order_form_shortcode'); ?>
                    <?php echo do_shortcode($short); ?>
                </div>
            </div>
    </div>
</section>
<!-- WANT TO MAKE AN ORDER -->


<!-- READ OUR RECENT ARTICLES -->
<section class="layout home-news pt-2 pb-5">
    <h2 class="heading-blue text-center"><?php the_field('news_carousel_title'); ?></h2>
    <div class="container layout__column home-news__slider">

        
        <?php $wpb_all_query = new WP_Query(array('post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => -1)); ?>
        <?php if ($wpb_all_query->have_posts()) : ?>
        <?php while ($wpb_all_query->have_posts()) : $wpb_all_query->the_post(); ?>

        <?php $blogbackgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>


            
            <div class="layout__4-col home-news__col">
            <?php if (!empty($blogbackgroundImg)) { ?>
            <a class="home-news__link" href="<?php the_permalink(); ?>"><img class="home-news__img" alt="Lila Jewellery News" src="<?php echo $blogbackgroundImg[0]; ?>"></a>
            <?php  } else { ?>
                <a class="home-news__link" href="<?php the_permalink(); ?>"><img class="home-news__img" alt="Lila Jewellery News" src="/wp-content/uploads/2022/04/post-placeholder.png" alt="Image Coming Soon"></a>
            <?php } ?>
                
                <a href="<?php the_permalink(); ?>"><h5 class="home-news__heading">
                    <?php 
                        $title = get_the_title(); 
                        if (strlen($title) > 55) {
                            $title = substr($title, 0, 55) . '...';
                         }
                         echo $title;
                    ?>
                </h5></a>
                <p><?php
                $content = get_field('blog_content_1');
                if (strlen($content) > 140) {
                    $content = substr($content, 0, 140) . '...';
                 }
                echo $content;
                ?></p>

            </div>

        <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p class="latest-news__nonews"><?php _e('Sorry, no news stories to display.'); ?></p>
            <?php endif; ?>

    </div>    

</section>
<!-- READ OUR RECENT ARTICLES -->

<?php get_footer(); ?>