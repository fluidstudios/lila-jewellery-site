<?php /* Template Name: lila-ContactPage */ ?>
<?php get_header(); ?>

<!-- BREADCRUMBS -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- BREADCRUMBS -->


<section class="generic-content contact">
    <div class="container generic-content__container contact__column">

        <div class="contact__col">
            
            <h1 class="heading-blue"><?php the_field('contact_heading'); ?></h1>
            <h5 class="contact__subheading"><?php the_field('contact_subheading'); ?></h5>

            <div class="contact__address-wrap">
                <div class="contact__address-left">
                    <i class="contact__icon fa-solid fa-phone"></i>
                </div>
                <div class="contact__address-right">
                    <?php $contact_telephone =  get_field('contact_telephone'); ?>
                    <?php $contact_telephone = str_replace(')', '', str_replace('(','',$contact_telephone)) ?>
                    <?php $contact_telephone = str_replace(' ', '',$contact_telephone ) ?>
                    <a class="contact__telephone" href="tel:<?php echo $contact_telephone; ?>"><?php the_field('contact_telephone'); ?></a>
                </div>
            </div>

            <div class="contact__address-wrap-2">
                <div class="contact__address-left">
                    <i class="contact__icon fa-solid fa-map-pin"></i>
                </div>
                <div class="contact__address-right">
                    <p class="contact__address">
                        <?php the_field('contact_address'); ?>
                    </p>
                </div>
            </div>

            <div class="home-contact">
                <div class="contact-form">
                    <?php $short = get_field('contact_form'); ?>
                    <?php echo do_shortcode($short); ?>
                </div>
            </div>
            

        </div>
        <div class="contact__col">
            <?php $contact_image = get_field('contact_image'); ?>
            <?php if(!empty(get_field('contact_image'))){ ?>
                <img class="contact__image" alt="<?php echo $contact_image['alt']; ?>" src="<?php echo $contact_image['url']; ?>">
            <?php } else { ?>
                <img class="contact__image" alt="<?php echo $contact_image['alt']; ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/assets/images/post-placeholder-4.png">
            <?php } ?>
            
        </div>
    

    </div>
</section>



<?php get_footer(); ?>