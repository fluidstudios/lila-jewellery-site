<?php include 'includes/logo.php'; ?>

<footer class="footer">

    <section class="footer__layout container">
        <div class="footer__col-1">
            <h5 class="footer__heading">SHOPPING</h5>
                <?php fluidpress_footer_menu(); ?>
        </div>
        <div class="footer__col-2">
            <h5 class="footer__heading">INFORMATION</h5>
                <?php fluidpress_footer_menu_2(); ?>
        </div>
        <div class="footer__col-3">
            <h5 class="footer__heading">ACCOUNT</h5>
            <?php fluidpress_footer_menu_3(); ?>
        </div>
        <div class="footer__col-4 text-center">
            <a href="/">
                <img src="<?= $logo->url ?>" alt="<?= $logo->alt ?>" class="footer__logo">
            </a>

            <?php //$short = '[contact-form-7 id="7739" title="Footer Newsletter Signup"]'; ?>
            <?php //echo do_shortcode($short); ?>

            <?php $short2 = '[mc4wp_form id="8419"]'; ?>
            <?php echo do_shortcode($short2); ?>

            

            <p class="footer__signoff"><?php the_field('newsletter_signup_footer_text', 'option'); ?></p>
        </div>
    </section>

    <section class="footer__bottom">
        <div class="container footer__container">
        <p>©Lila Jewellery <?php echo date("Y"); ?>. <?php the_field('footer_copyright_line', 'option'); ?></p>
            <?php if(!empty(get_field('social_media_link', 'option'))){ ?>
                <a class="footer__social-link" target="_blank" href="<?php the_field('footer_social_link', 'option'); ?>"><i class="fab <?php the_field('footer_social_icon', 'option'); ?>"></i></a>
            <?php } ?>
        </div>
    </section>
</footer>

<?php wp_footer(); ?>

</body>

</html>