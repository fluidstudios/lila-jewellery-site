<?php get_header(); ?>

<!-- BREADCRUMBS -->
<?php include(locate_template('./template-parts/breadcrumbs.php')); ?>
<!-- BREADCRUMBS -->

<!-- PAGE TITLE -->
<?php include(locate_template('./template-parts/page-title.php')); ?>
<!-- PAGE TITLE -->

    <section class="generic-content">
        <div class="container container generic-content__container">
            <?php the_content(); ?>
        </div>
    </section>

<?php get_footer(); ?>