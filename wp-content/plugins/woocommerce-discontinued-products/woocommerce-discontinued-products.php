<?php
/**
 * The main plugin file for WooCommerce Discontinued Products
 *
 * This file is included during the WordPress bootstrap process if the plugin is active.
 *
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 *
 * @wordpress-plugin
 * Plugin Name:     WooCommerce Discontinued Products
 * Plugin URI:      https://barn2.com/wordpress-plugins/woocommerce-discontinued-products
 * Description:     Provides an easy way to mark products as discontinued and choose whether to display them in your store.
 * Version:         1.0.5
 * Author:          Barn2 Plugins
 * Author URI:      https://barn2.com
 * Text Domain:     woocommerce-discontinued-products
 * Domain Path:     /languages
 *
 * WC requires at least: 3.7
 * WC tested up to: 6.4
 *
 * Copyright:       Barn2 Media Ltd
 * License:         GNU General Public License v3.0
 * License URI:     http://www.gnu.org/licenses/gpl-3.0.html
 */

namespace Barn2\Plugin\WC_Discontinued_Products;

defined( 'ABSPATH' ) || exit;

const PLUGIN_VERSION = '1.0.5';
const PLUGIN_FILE    = __FILE__;

require dirname( __FILE__ ) . '/vendor/autoload.php';

/**
 * Helper function to access the shared plugin instance.
 *
 * @return Plugin
 */
function wdp() {
	return Plugin_Factory::create( PLUGIN_FILE, PLUGIN_VERSION );
}

wdp()->register();
