<?php // phpcs:ignore WordPress.Files.FileName
/**
 * The main plugin class. Responsible for setting up to core plugin services.
 *
 * @package   Barn2\woocommerce-private-store
 * @author    Barn2 Plugins <support@barn2.com>
 * @copyright Barn2 Media Ltd
 * @license   GPL-3.0
 */

namespace Barn2\Plugin\WC_Discontinued_Products;

use Barn2\Plugin\WC_Discontinued_Products\Admin\Admin_Controller;
use Barn2\Plugin\WC_Discontinued_Products\Admin\Plugin_Setup;
use Barn2\Plugin\WC_Discontinued_Products\Integration\Lead_Time;
use Barn2\Plugin\WC_Discontinued_Products\Integration\Theme_Bridge;
use Barn2\Plugin\WC_Discontinued_Products\Integration\Theme_Jupiterx;
use Barn2\Plugin\WC_Discontinued_Products\Integration\Theme_Shopkeeper;
use Barn2\Plugin\WC_Discontinued_Products\Integration\Theme_Uncode;
use Barn2\WDP_Lib\Admin\Notices;
use Barn2\WDP_Lib\Plugin\Licensed_Plugin;
use Barn2\WDP_Lib\Plugin\Premium_Plugin;
use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Service_Container;
use Barn2\WDP_Lib\Service_Provider;
use Barn2\WDP_Lib\Translatable;
use Barn2\WDP_Lib\Util;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Boot the plugin.
 */
class Plugin extends Premium_Plugin implements Licensed_Plugin, Registerable, Translatable, Service_Provider {

	use Service_Container;

	const NAME    = 'WooCommerce Discontinued Products';
	const ITEM_ID = 349484;

	// ID used when saving the status into the database.
	const DISCONTINUED_ID = 'discontinued';

	/**
	 * Constructs and initalizes the main plugin class.
	 *
	 * @param string $file The main plugin file.
	 * @param string $version The current plugin version.
	 */
	public function __construct( $file = null, $version = '1.0.0' ) {
		parent::__construct(
			[
				'name'               => self::NAME,
				'item_id'            => self::ITEM_ID,
				'version'            => $version,
				'file'               => $file,
				'is_woocommerce'     => true,
				'settings_path'      => 'admin.php?page=wc-settings&tab=products&section=discontinued-products',
				'documentation_path' => 'kb-categories/wdp-kb/',
			]
		);
	}

	/**
	 * Hook into WordPress
	 *
	 * @return void
	 */
	public function register() {
		parent::register();
		add_action( 'init', [ $this, 'load_services' ] );
		add_action( 'init', [ $this, 'load_textdomain' ] );

		// We create Plugin_Setup here so the plugin activation hook will run.
		$plugin_setup = new Plugin_Setup( $this->get_file() );
		$plugin_setup->register();
	}

	/**
	 * Register plugin services
	 *
	 * @return void
	 */
	public function load_services() {
		// Don't load anything if WooCommerce not active.
		if ( ! Util::is_woocommerce_active() ) {
			$this->add_missing_woocommerce_notice();
			return;
		}

		$this->register_services();
	}

	/**
	 * Get list of services
	 *
	 * @return array
	 */
	public function get_services() {
		$services = [];

		if ( Util::is_admin() ) {
			$services['admin'] = new Admin_Controller( $this );
		}

		if ( $this->get_license()->is_valid() ) {
			$services['stock_display']                = new Stock_Display( $this );
			$services['stock_handler']                = new Stock_Handler();
			$services['import_export']                = new Import_Export();
			$services['integration_lead_time']        = new Lead_Time();
			$services['integration_theme_shopkeeper'] = new Theme_Shopkeeper();
			$services['integration_theme_bridge']     = new Theme_Bridge();
			$services['integration_theme_jupiterx']   = new Theme_Jupiterx();
			$services['integration_theme_uncode']     = new Theme_Uncode();
		}

		return $services;
	}

	/**
	 * Make plugin translatable
	 *
	 * @return void
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'woocommerce-discontinued-products', false, $this->get_slug() . '/languages' );
	}

	/**
	 * Display notice if WC is missing
	 *
	 * @return void
	 */
	private function add_missing_woocommerce_notice() {
		if ( Util::is_admin() ) {
			$admin_notice = new Notices();
			$admin_notice->add(
				'wdp_woocommerce_missing',
				'',
				sprintf(
					__( 'Please %1$sinstall WooCommerce%2$s in order to use WooCommerce Discontinued Products.', 'woocommerce-discontinued-products' ),
					Util::format_link_open( 'https://woocommerce.com/', true ),
					'</a>'
				),
				[
					'type'       => 'error',
					'capability' => 'install_plugins'
				]
			);
			$admin_notice->boot();
		}
	}

}
