<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products;

use Barn2\WDP_Lib\Registerable;

/**
 * Handles import and export of discontinued products.
 */
class Import_Export implements Registerable {
	/**
	 * Run the integration.
	 *
	 * @return void
	 */
	public function register() {
		$this->init();
	}

	/**
	 * Hook into WC.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( 'woocommerce_product_export_column_names', [ $this, 'add_export_column' ] );
		add_filter( 'woocommerce_product_export_product_default_columns', [ $this, 'add_export_column' ] );

		add_filter( 'woocommerce_csv_product_import_mapping_options', [ $this, 'add_columns_to_importer' ] );
		add_filter( 'woocommerce_csv_product_import_mapping_default_columns', [ $this, 'add_column_to_mapping_screen' ] );

		add_filter( 'woocommerce_product_export_product_column_is_discontinued', [ $this, 'add_export_data_is_discontinued' ], 10, 2 );

		add_filter( 'woocommerce_product_import_pre_insert_product_object', [ $this, 'process_import' ], 10, 2 );
	}

	/**
	 * Add a new column to the importer.
	 *
	 * @param array $options
	 * @return array
	 */
	public function add_columns_to_importer( $options ) {
		$options['is_discontinued'] = 'Is discontinued?';
		return $options;
	}

	/**
	 * Automatically map the new column inside the importer.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function add_column_to_mapping_screen( $columns ) {
		$columns['Is discontinued?'] = 'is_discontinued';
		return $columns;
	}

	/**
	 * Add a new column to the exporter.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function add_export_column( $columns ) {
		$columns['is_discontinued'] = 'Is discontinued?';
		return $columns;
	}

	/**
	 * Display the value of the custom export column.
	 *
	 * @param string $value
	 * @param object $product
	 * @return string
	 */
	public function add_export_data_is_discontinued( $value, $product ) {
		return $product->get_stock_status() === Plugin::DISCONTINUED_ID ? 'yes' : '';
	}

	/**
	 * Process products on import and set the appropriate stock status.
	 *
	 * @param object $object product object
	 * @param array $data processed data
	 * @return object
	 */
	public function process_import( $object, $data ) {
		if ( isset( $data['is_discontinued'] ) ) {
			if ( $data['is_discontinued'] === 'yes' ) {
				$object->set_stock_status( Plugin::DISCONTINUED_ID );
			} elseif ( $data['is_discontinued'] !== 'yes' && isset( $data['stock_status'] ) ) {
				$object->set_stock_status( $data['stock_status'] );
			}
		}

		return $object;
	}

}
