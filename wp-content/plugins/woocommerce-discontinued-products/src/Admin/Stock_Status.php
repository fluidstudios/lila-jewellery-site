<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Admin;

use Barn2\Plugin\WC_Discontinued_Products\Plugin;
use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Service;

/**
 * Handles registration of hooks triggered in the admin panel
 * that show the discontinued status of products.
 */
class Stock_Status implements Registerable, Service {

	/**
	 * Hook into WP
	 *
	 * @return void
	 */
	public function register() {
		add_filter( 'woocommerce_admin_stock_html', [ $this, 'render_admin_stock_html' ], 10, 2 );
	}

	/**
	 * Render the discontinued status within the WC products admin list table.
	 *
	 * @param string $stock_html
	 * @param object $product WC Product class
	 * @return string
	 */
	public function render_admin_stock_html( $stock_html, $product ) {

		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID ) {
			$stock_html = '<mark class="outofstock discontinued">' . esc_html__( 'Discontinued', 'woocommerce-discontinued-products' ) . '</mark>';
		}

		return $stock_html;

	}
}
