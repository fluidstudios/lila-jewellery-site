<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Admin;

use Barn2\WDP_Lib\Plugin\Licensed_Plugin;
use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Service;
use Barn2\WDP_Lib\Util;
use Barn2\WDP_Lib\WooCommerce\Admin\Custom_Settings_Fields;
use Barn2\WDP_Lib\WooCommerce\Admin\Plugin_Promo;
use WC_Barn2_Plugin_Promo;

/**
 * Register settings for the discontinued products.
 */
class Settings_Page implements Registerable, Service {

	private $id;
	private $label;
	private $plugin;
	private $license_setting;

	public function __construct( Licensed_Plugin $plugin ) {
		$this->id              = 'discontinued-products';
		$this->label           = esc_html__( 'Discontinued products', 'woocommerce-discontinued-products' );
		$this->plugin          = $plugin;
		$this->license_setting = $plugin->get_license_setting();
	}

	/**
	 * Register settings and sections
	 *
	 * @return void
	 */
	public function register() {

		$settings_fields = new Custom_Settings_Fields();
		$settings_fields->register();

		add_filter( 'woocommerce_get_sections_products', [ $this, 'register_settings_section' ], 10, 1 );
		add_filter( 'woocommerce_get_settings_products', [ $this, 'get_settings' ], 10, 2 );

		// Sanitize options on save.
		add_filter( 'woocommerce_admin_settings_sanitize_option', [ $this, 'sanitize_options' ], 10, 3 );

		// Add the plugin promo.
		//if ( class_exists( 'WC_Barn2_Plugin_Promo' ) ) {
		//	$promo = new WC_Barn2_Plugin_Promo( $this->plugin->get_id(), $this->plugin->get_file(), $this->id );
		//	$promo->register();
		//}

		$promo = new Plugin_Promo( $this->plugin->get_id(), $this->plugin->get_file(), $this->id );
		$promo->register();

	}

	/**
	 * Register the settings section.
	 *
	 * @param array $sections
	 * @return array $sections
	 */
	public function register_settings_section( $sections ) {
		$sections[ $this->id ] = $this->label;

		return $sections;
	}

	/**
	 * Register settings for the discontinued products functionality.
	 *
	 * @param array  $settings
	 * @param string $current_section
	 * @return array
	 */
	public function get_settings( $settings, $current_section ) {

		if ( $this->id !== $current_section ) {
			return $settings;
		}

		$custom_settings = [
			[
				'id'    => 'discontinued_products_settings_start',
				'type'  => 'settings_start',
				'class' => 'barn2-plugins-settings'
			],
			[
				'name' => __( 'Discontinued products', 'woocommerce-discontinued-products' ),
				'type' => 'title',
				'desc' => '<p>' . __( 'The following options control the WooCommerce Discontinued Products extension.', 'woocommerce-discontinued-products' ) . '</p>' .
						  '<p>' .
						  Util::format_link( $this->plugin->get_documentation_url(), __( 'Documentation', 'woocommerce-discontinued-products' ) ) . ' | ' .
						  Util::format_link( $this->plugin->get_support_url(), __( 'Support', 'woocommerce-discontinued-products' ) ) .
						  '</p>',
				'id'   => 'wcdp_settings'
			],
			$this->license_setting->get_license_key_setting(),
			$this->license_setting->get_license_override_setting(),
			[
				'name'     => esc_html__( 'Discontinued text', 'woocommerce-discontinued-products' ),
				'type'     => 'text',
				'desc'     => esc_html__( 'This is used for the stock status which appears on the front end.', 'woocommerce-discontinued-products' ),
				'default'  => 'Discontinued',
				'desc_tip' => true,
				'id'       => 'wcdp_text',
			],
			[
				'id'      => 'wcdp_hide_from_store',
				'name'    => esc_html__( 'Hide products', 'woocommerce-discontinued-products' ),
				'type'    => 'checkbox',
				'default' => 'yes',
				'desc'    => __( 'Hide discontinued products from the store', 'woocommerce-discontinued-products' ),
			],
			[
				'type' => 'sectionend',
				'id'   => 'wcdp_settings',
			],
			[
				'id'   => 'discontinued_products_settings_end',
				'type' => 'settings_end'
			]
		];

		return apply_filters( "woocommerce_get_settings_{$this->id}", $custom_settings );

	}

	public function sanitize_options( $value, $option, $raw_value ) {
		if ( empty( $option['id'] ) ) {
			return $value;
		}

		switch ( $option['id'] ) {
			case $this->license_setting->get_license_setting_name():
				$value = $this->license_setting->save_license_key( $value );
				break;
		}

		return $value;
	}

}
