<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Admin;

use Barn2\WDP_Lib\Plugin\Admin\Admin_Links;
use Barn2\WDP_Lib\Plugin\Licensed_Plugin;
use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Service;
use Barn2\WDP_Lib\Service_Container;
use Barn2\WDP_Lib\Util;
use Barn2\WDP_Lib\WooCommerce\Admin\Navigation;

/**
 * Setup admin services.
 *
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */
class Admin_Controller implements Registerable, Service {

	use Service_Container;

	public function __construct( Licensed_Plugin $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * List of admin services
	 *
	 * @return array
	 */
	public function get_services() {
		return [
			'admin_links'        => new Admin_Links( $this->plugin ),
			'settings_page'      => new Settings_Page( $this->plugin ),
			'wc_navigation'      => new Navigation( $this->plugin, 'products&section=discontinued-products', esc_html__( 'Discontinued Products', 'woocommerce-discontinued-products' ) ),
			'admin_stock_status' => new Stock_Status(),
		];
	}

	/**
	 * Register services and hooks
	 *
	 * @return void
	 */
	public function register() {
		$this->register_services();

		add_action( 'admin_enqueue_scripts', [ $this, 'load_scripts' ] );
	}

	/**
	 * Load assets required in the products page.
	 *
	 * @return void
	 */
	public function load_scripts() {

		$screen = get_current_screen();

		if ( $screen->id !== 'product' ) {
			return;
		}

		$suffix = Util::get_script_suffix();

		wp_register_script( 'wcdp-admin', $this->plugin->get_dir_url() . "assets/js/admin/wcdp-admin{$suffix}.js", [ 'jquery' ], $this->plugin->get_version(), true );

		wp_enqueue_script( 'wcdp-admin' );

		wp_add_inline_script(
			'wcdp-admin',
			'const wcdp_settings = ' . wp_json_encode(
				[
					'ajaxurl'             => admin_url( 'admin-ajax.php' ),
					'nonce'               => wp_create_nonce( 'wcdp-nonce' ),
					'discontinued_option' => esc_html__( 'Set Status - Discontinued', 'woocommerce-discontinued-products' ),
				]
			),
			'before'
		);

		wp_enqueue_style( 'wcdp-admin' );

	}

}
