<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Admin;

use Barn2\Plugin\WC_Discontinued_Products\Util as WPD_Util;
use Barn2\WDP_Lib\Plugin\Plugin_Activation_Listener;
use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Util;

/**
 * Setup the plugin on activation
 *
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */
class Plugin_Setup implements Plugin_Activation_Listener, Registerable {

	/**
	 * Plugin's entry file
	 *
	 * @var string
	 */
	private $file;

	/**
	 * Get things started
	 *
	 * @param string $file
	 */
	public function __construct( $file ) {
		$this->file = $file;
	}

	/**
	 * Register the service
	 *
	 * @return void
	 */
	public function register() {
		register_activation_hook( $this->file, [ $this, 'on_activate' ] );
		register_deactivation_hook( $this->file, [ $this, 'on_deactivate' ] );

		add_action( 'admin_init', [ $this, 'after_plugin_activation' ] );
	}

	/**
	 * On plugin activation install default values.
	 *
	 * @return void
	 */
	public function on_activate() {
		// Network wide.
		// phpcs:disable
		$network_wide = ! empty( $_GET['networkwide'] )
			? (bool) $_GET['networkwide']
			: false;
		// phpcs:enable

		// Call the installer directly during the activation hook.
		$this->setup( $network_wide );
	}

	/**
	 * On plugin deactivation mark all discontinued products as outofstock.
	 *
	 * @return void
	 */
	public function on_deactivate() {

		if ( ! Util::is_woocommerce_active() ) {
			return;
		}

		WPD_Util::set_discontinued_products_to_outofstock();

	}

	/**
	 * Detect how to install the plugin.
	 * If the plugin is installed network wide on a multisite setup,
	 * make sure subsites run the installation too.
	 *
	 * @param boolean $network_wide
	 * @return void
	 */
	public function setup( $network_wide = false ) {
		global $wpdb;
		if ( is_multisite() && $network_wide ) {
			foreach ( $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs LIMIT 100" ) as $blog_id ) { //phpcs:ignore
				switch_to_blog( $blog_id );
				$this->install();
				restore_current_blog();
			}
		} else {
			$this->install();
		}
	}

	/**
	 * Install default options of the plugin.
	 *
	 * @return void
	 */
	public function install() {
		if ( ! get_option( 'wcdp_text' ) ) {
			update_option( 'wcdp_text', 'Discontinued' );
		}

		if ( ! get_option( 'wcdp_hide_from_store' ) ) {
			update_option( 'wcdp_hide_from_store', 'yes' );
		}

		// Add the transient to redirect.
		set_transient( '_wdp_activation_redirect', true, 30 );

	}

	/**
	 * Detect the transient and update products if needed.
	 *
	 * Because our custom "discontinued" stock status does not
	 * exist yet during the register_activation_hook, we have
	 * to move this on admin_init
	 *
	 * @return void
	 */
	public function after_plugin_activation() {

		// Bail if no activation redirect.
		if ( ! get_transient( '_wdp_activation_redirect' ) ) {
			return;
		}

		delete_transient( '_wdp_activation_redirect' );

		if ( Util::is_woocommerce_active() ) {
			WPD_Util::set_outofstock_products_back_to_discontinued();
		}

	}
}
