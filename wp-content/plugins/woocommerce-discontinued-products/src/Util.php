<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products;

/**
 * Collection of helper methods used throughout the plugin.
 */
class Util {

	/**
	 * Determine if all given variations of a product are discontinued.
	 *
	 * @param \WC_Product_Variable $product
	 * @return void
	 */
	public static function variations_are_discontinued( $product ) {
		if ( $product instanceof \WC_Product_Variable ) {
			$variations = self::get_product_variations( $product );

			if ( empty( $variations ) ) {
				return false;
			}

			$available_variations_count    = count( $variations );
			$discontinued_variations_count = 0;

			foreach ( $variations as $variation ) {
				if ( $variation instanceof \WC_Product_Variation && $variation->get_stock_status() === Plugin::DISCONTINUED_ID ) {
					$discontinued_variations_count++;
				}
			}

			return $available_variations_count === $discontinued_variations_count;
		}

		return false;
	}

	/**
	 * Get variations of a product.
	 *
	 * We need a custom way to retrieve variations because WooCommerce
	 * can only retrieve available variations but discontinued variations
	 * are marked as "unavailable" by this extension under certain conditions.
	 *
	 * This code is a slightly modified version of WC's get_available_variation's method.
	 *
	 * @param object $product WC Product class
	 * @return array
	 */
	public static function get_product_variations( $product ) {

		$variation_ids        = $product->get_children();
		$available_variations = [];

		if ( empty( $variation_ids ) ) {
			return [];
		}

		if ( is_callable( '_prime_post_caches' ) ) {
			_prime_post_caches( $variation_ids );
		}

		foreach ( $variation_ids as $variation_id ) {
			$variation = wc_get_product( $variation_id );

			if ( ! $variation ) {
				continue;
			}

			$available_variations[] = $variation;
		}

		return $available_variations;

	}

	/**
	 * Determine if the lead time extension is enabled.
	 *
	 * @return boolean
	 */
	public static function is_lead_time_plugin_active() {
		return function_exists( '\Barn2\Plugin\WC_Lead_Time\wlt' );
	}

	/**
	 * Determine if the Quick View Pro is enabled.
	 *
	 * @return boolean
	 */
	public static function is_quick_view_pro_plugin_active() {
		return function_exists( '\Barn2\Plugin\WC_Quick_View_Pro\wqv' );
	}

	/**
	 * Determine if the bulk variations extension is enabled.
	 *
	 * @deprecated 1.0.1
	 * @return boolean
	 */
	public static function is_bulk_variations_plugin_active() {

		if ( function_exists( '\Barn2\Plugin\WC_Bulk_Variations\wbv' ) ) {
			return \Barn2\Plugin\WC_Bulk_Variations\wbv()->has_valid_license();
		}

		return false;

	}

	/**
	 * Insert an array at a specific index
	 *
	 * @param array $array
	 * @param int $position
	 * @param array $insert
	 * @return array
	 */
	public static function array_insert( $array, $position, $insert ) {
		if ( $position > 0 ) {
			if ( $position == 1 ) {
				array_unshift( $array, [] );
			} else {
				$position = $position - 1;
				array_splice(
					$array,
					$position,
					0,
					[
						''
					]
				);
			}
			$array[ $position ] = $insert;
		}

		return $array;
	}

	/**
	 * Move discontinued products to the out of stock status.
	 * Track the products via post meta too so that we know
	 * these products were once discontinued.
	 *
	 * This is used during plugin deactivation.
	 *
	 * @return void
	 */
	public static function set_discontinued_products_to_outofstock() {

		/**
		 * This action is fired once, before discontinued products are
		 * queried and their stock status is changed to out of stock.
		 */
		do_action( 'wdp_before_set_discontinued_products_to_outofstock' );

		$products = wc_get_products(
			[
				'limit'        => -1,
				'stock_status' => Plugin::DISCONTINUED_ID,
			]
		);

		if ( ! empty( $products ) ) {
			foreach ( $products as $product ) {
				update_post_meta( $product->get_id(), '_was_discontinued', 'yes' );
				$product->set_stock_status( 'outofstock' );
				$product->save();
			}
		}

		// Now query variations individually ( when not all variations are discontinued, the product is technically "in stock" ).
		$variations = wc_get_products(
			[
				'limit'        => -1,
				'stock_status' => Plugin::DISCONTINUED_ID,
				'type'         => 'variation'
			]
		);

		if ( ! empty( $variations ) ) {
			foreach ( $variations as $variation ) {
				update_post_meta( $variation->get_id(), '_was_discontinued', 'yes' );
				$variation->set_stock_status( 'outofstock' );
				$variation->save();
			}
		}

		/**
		 * This action is fired once, after discontinued products are
		 * queried and after their stock status has already been changed to out of stock.
		 *
		 * @param array $products list of discontinued products that have been found.
		 * @param array $variations list of individual discontinued variations that have been found.
		 */
		do_action( 'wdp_after_set_discontinued_products_to_outofstock', $products, $variations );

	}

	/**
	 * Move products that were once discontinued back to the discontinued status.
	 *
	 * This is used during plugin activation to check for existing
	 * previously discontinued products.
	 *
	 * @return void
	 */
	public static function set_outofstock_products_back_to_discontinued() {

		/**
		 * This action is fired once, before the previously discontinued and currently out of stock -
		 * products are queried and their stock status is changed back to discontinued.
		 */
		do_action( 'wdp_before_set_outofstock_products_back_to_discontinued' );

		$products = wc_get_products(
			[
				'limit'        => -1,
				'stock_status' => 'outofstock',
				'meta_key'     => '_was_discontinued',
			]
		);

		if ( ! empty( $products ) ) {
			foreach ( $products as $product ) {
				delete_post_meta( $product->get_id(), '_was_discontinued' );
				$product->set_stock_status( Plugin::DISCONTINUED_ID );
				$product->save();
			}
		}

		// Now query variations individually ( when not all variations are discontinued, the product is technically "in stock" ).
		$variations = wc_get_products(
			[
				'limit'        => -1,
				'stock_status' => 'outofstock',
				'type'         => 'variation',
				'meta_key'     => '_was_discontinued',
			]
		);

		if ( ! empty( $variations ) ) {
			foreach ( $variations as $variation ) {
				delete_post_meta( $variation->get_id(), '_was_discontinued' );
				$variation->set_stock_status( Plugin::DISCONTINUED_ID );
				$variation->save();
			}
		}

		// Query for variable products and trigger the save method
		// again to sync and persist the discontinued status
		// via the woocommerce_before_product_object_save hook.
		$variable_products = wc_get_products(
			[
				'limit' => -1,
				'type'  => 'variable',
			]
		);

		if ( ! empty( $variable_products ) ) {
			foreach ( $variable_products as $vproduct ) {
				delete_post_meta( $vproduct->get_id(), '_was_discontinued' );
				$vproduct->save();
			}
		}

		/**
		 * This action is fired once, after the out of stock products have had
		 * their stock status changed back to discontinued.
		 *
		 * @param array $products list of queried and previously out of stock products
		 * @param array $variations list of individual variations previously out of stock
		 * @param array $variable_products list of individual variable products that may have been out of stock
		 */
		do_action( 'wdp_after_set_outofstock_products_back_to_discontinued', $products, $variations, $variable_products );
	}
}
