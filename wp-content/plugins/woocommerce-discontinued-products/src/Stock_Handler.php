<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products;

use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Service;

/**
 * Handles restrictions and query modifications
 * for the new discontinued status.
 */
class Stock_Handler implements Registerable, Service {

	/**
	 * Hook into WP
	 *
	 * @return void
	 */
	public function register() {
		add_filter( 'woocommerce_product_is_in_stock', [ $this, 'is_in_stock' ], 10, 2 );
		add_filter( 'woocommerce_variation_is_visible', [ $this, 'disable_variation_in_dropdown' ], 10, 4 );
		add_filter( 'woocommerce_variable_children_args', [ $this, 'variable_children_args' ], 10, 3 );
		add_action( 'woocommerce_product_query_meta_query', [ $this, 'hide_from_store' ], 10, 2 );
		add_filter( 'woocommerce_related_products', [ $this, 'exclude_discontinued_from_related' ], 10, 3 );
		add_action( 'woocommerce_before_product_object_save', [ $this, 'discontinue_parent_product' ], 10 );
		add_action( 'woocommerce_bulk_edit_variations', [ $this, 'bulk_edit_variations' ], 10, 4 );
	}

	/**
	 * Filter whether the product is in stock or not
	 *
	 * @param bool   $is_in_stock
	 * @param object $product WooCommerce product class
	 * @return bool
	 */
	public function is_in_stock( $is_in_stock, $product ) {
		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID ) {
			$is_in_stock = false;
		}

		return $is_in_stock;
	}

	/**
	 * Hide discontinued variations from the dropdown on the frontend.
	 *
	 * @param bool $visible
	 * @param string $variation_id
	 * @param string $parent_id
	 * @param object $variation
	 * @return bool
	 */
	public function disable_variation_in_dropdown( $visible, $variation_id, $parent_id, $variation ) {

		if ( $variation->get_stock_status() === Plugin::DISCONTINUED_ID && get_option( 'wcdp_hide_from_store' ) === 'yes' ) {
			return false;
		}

		return $visible;

	}

	/**
	 * Hide discontinued products from shop page when the setting is enabled.
	 *
	 * @param array         $meta_query list of meta queries configs
	 * @param boolean|oject $query the query
	 * @return array
	 */
	public function hide_from_store( $meta_query = [], $query = false ) {

		if ( get_option( 'wcdp_hide_from_store' ) === 'yes' ) {
			$meta_query[] = [
				'key'     => '_stock_status',
				'value'   => Plugin::DISCONTINUED_ID,
				'compare' => '!=',
			];
		}

		return $meta_query;

	}

	/**
	 * Exclude discontinued products from the related query.
	 *
	 * @param array $related_posts
	 * @param string $product_id
	 * @param array $args
	 * @return void
	 */
	public function exclude_discontinued_from_related( $related_posts, $product_id, $args ) {

		if ( get_option( 'wcdp_hide_from_store' ) === 'yes' ) {
			$exclude_ids = wc_get_products(
				[
					'status'       => 'publish',
					'limit'        => -1,
					'stock_status' => Plugin::DISCONTINUED_ID,
					'return'       => 'ids',
				]
			);

			if ( ! empty( $exclude_ids ) ) {
				return array_diff( $related_posts, $exclude_ids );
			}
		}

		return $related_posts;

	}

	/**
	 * Persist the "discontinued" status on the parent product of a variable product.
	 * Because the stock status sync is done upwards from child(variation) to parent
	 * we override the stock status at the last minute before the parent product is updated.
	 *
	 * @param object $product WC Product class
	 * @return void
	 */
	public function discontinue_parent_product( $product ) {

		if ( $product->get_type() !== 'variable' ) {
			return;
		}

		$variations = Util::get_product_variations( $product );

		if ( empty( $variations ) ) {
			return;
		}

		$available_variations_count    = count( $variations );
		$discontinued_variations_count = 0;

		foreach ( $variations as $variation ) {
			if ( $variation instanceof \WC_Product_Variation && $variation->get_stock_status() === Plugin::DISCONTINUED_ID ) {
				$discontinued_variations_count++;
			}
		}

		if ( $available_variations_count === $discontinued_variations_count ) {
			$product->set_stock_status( Plugin::DISCONTINUED_ID );
		}
	}

	/**
	 * Filter the query arguments of the query returning the the visible children of a variable product.
	 *
	 * @param array      $visible_only_args The array with the query arguments
	 * @param \WC_Product $product           The current product
	 * @param bool       $visible_only      Whether only visible children should be returned or not
	 * @return array                        The filtered query arguments
	 */
	public function variable_children_args( $visible_only_args, $product, $visible_only ) {
		if ( $visible_only && 'yes' === get_option( 'wcdp_hide_from_store' ) ) {
			$visible_only_args['meta_query'][] = [
				'key'     => '_stock_status',
				'value'   => Plugin::DISCONTINUED_ID,
				'compare' => '!=',
			];
		}
		return $visible_only_args;
	}

	/**
	 * Discontinue variations in bulk when triggered.
	 *
	 * @param string $bulk_action name of the action being triggered.
	 * @param array $data data sent through the request.
	 * @param string $product_id
	 * @param array $variations
	 * @return void
	 */
	public function bulk_edit_variations( $bulk_action, $data, $product_id, $variations ) {

		if ( $bulk_action !== 'variable_stock_status_discontinued' ) {
			return;
		}

		/**
		 * This action is fired once before all variations of a variable product
		 * are discontinued by pressing the "Discontinue all variations" button.
		 *
		 * @param int $product_id id number of the product for which variations are being discontinued.
		 */
		do_action( 'wdp_before_ajax_variations_discontinued', $product_id );

		$product    = new \WC_Product_Variable( $product_id );
		$variations = Util::get_product_variations( $product );

		if ( empty( $variations ) ) {
			return;
		}

		// Disable stock management.
		$product->set_manage_stock( false );
		$product->save();

		foreach ( $variations as $variation ) {
			if ( $variation instanceof \WC_Product_Variation ) {
				$variation->set_stock_status( Plugin::DISCONTINUED_ID );
				$variation->save();
			}
		}

		/**
		 * This action is fired once after all variations of a variable product
		 * are discontinued by pressing the "Discontinue all variations" button.
		 *
		 * @param \WC_Product_Variable $product product object
		 * @param array $variations list of variations found
		 */
		do_action( 'wdp_after_ajax_variations_discontinued', $product, $variations );

	}

}
