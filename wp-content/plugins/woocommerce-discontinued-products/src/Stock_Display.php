<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products;

use Barn2\WDP_Lib\Plugin\Licensed_Plugin;
use Barn2\WDP_Lib\Registerable;
use Barn2\WDP_Lib\Service;

/**
 * Handles registration of the new discontinued stock status
 * plus it handles the display of the stock status in various
 * areas of the frontend.
 */
class Stock_Display implements Registerable, Service {

	private $plugin;

	public function __construct( Licensed_Plugin $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 * Hook into WordPress
	 *
	 * @return void
	 */
	public function register() {
		add_filter( 'woocommerce_product_stock_status_options', [ $this, 'register_new_stock_status' ] );
		add_filter( 'woocommerce_get_availability_text', [ $this, 'get_availability_text' ], 10, 2 );
		add_filter( 'woocommerce_get_availability_class', [ $this, 'get_availability_css_class' ], 10, 2 );
		add_filter( 'woocommerce_out_of_stock_message', [ $this, 'get_variable_product_availability_text' ] );
	}

	/**
	 * Register the "Discontinued" stock status option.
	 *
	 * @param array $options
	 * @return array
	 */
	public function register_new_stock_status( $options ) {

		$options[ Plugin::DISCONTINUED_ID ] = esc_html__( 'Discontinued', 'woocommerce-discontinued-products' );

		return $options;

	}

	/**
	 * Displays a custom availability text when products are discontinued.
	 *
	 * @param string $availability
	 * @param object $product WooCommerce product class
	 * @return string
	 */
	public function get_availability_text( $availability, $product ) {

		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID ) {
			$availability = get_option( 'wcdp_text', esc_html__( 'Discontinued', 'woocommerce-discontinued-products' ) );
		}

		return $availability;

	}

	/**
	 * Setup a custom css class for the availability text displayed on the fronted.
	 *
	 * @param string $class
	 * @param object $product WooCommerce product class
	 * @return string
	 */
	public function get_availability_css_class( $class, $product ) {

		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID ) {
			$class = 'out-of-stock discontinued';
		}

		return $class;

	}

	/**
	 * Display the discontinued message for variable products.
	 *
	 * @return void
	 */
	public function get_variable_product_availability_text( $text ) {

		global $product;

		if ( $product instanceof \WC_Product_Variable ) {
			$variations = Util::get_product_variations( $product );

			if ( empty( $variations ) ) {
				return $text;
			}

			$available_variations_count    = count( $variations );
			$discontinued_variations_count = 0;

			foreach ( $variations as $variation ) {
				if ( $variation instanceof \WC_Product_Variation && $variation->get_stock_status() === Plugin::DISCONTINUED_ID ) {
					$discontinued_variations_count++;
				}
			}

			if ( $available_variations_count === $discontinued_variations_count ) {
				return get_option( 'wcdp_text', esc_html__( 'Discontinued', 'woocommerce-discontinued-products' ) );
			}
		}

		return $text;

	}

}
