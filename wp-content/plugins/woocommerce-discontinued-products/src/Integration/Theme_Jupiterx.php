<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Integration;

use Barn2\Plugin\WC_Discontinued_Products\Util;
use Barn2\WDP_Lib\Registerable;

/**
 * Adds custom integration support for the jupiterx theme.
 *
 * The theme doesn't use native WC hooks and so we can't display
 * our discontinued text without disabling the Jupiterx's badge
 * and then injecting our custom badge.
 */
class Theme_Jupiterx implements Registerable {

	public function register()
	{
		$theme = wp_get_theme();

		if ( $theme->template !== 'jupiterx' ) {
			return;
		}

		add_filter( 'theme_mod_jupiterx_product_page_custom_out_of_stock_badge', [ $this, 'display_default_badge' ] );
		add_action( 'jupiterx_product_page_badges', [ $this, 'add_discontinued_badge' ] );
	}

	public function display_default_badge( $mod ) {
		global $product;

		if ( is_string( $product ) ) {

			$the_product = get_page_by_path( $product, OBJECT, 'product' );
			$the_product = wc_get_product( $the_product );

			if ( Util::variations_are_discontinued( $the_product ) ) {
				return false;
			}
		}

		return $mod;
	}

	public function add_discontinued_badge() {
		global $product;

		if ( ! Util::variations_are_discontinued( $product ) ) {
			return;
		}

		echo wp_kses( '<span class="jupiterx-out-of-stock discontinued">' . get_option( 'wcdp_text', esc_html__( 'Discontinued', 'woocommerce-discontinued-products' ) ) . '</span>', [
			'span' => [
				'class' => [],
				'style' => [],
			],
		] );
	}

}
