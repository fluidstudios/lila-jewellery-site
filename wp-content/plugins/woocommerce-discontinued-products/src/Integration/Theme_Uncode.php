<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Integration;

/**
 * Custom integration for the bridget theme because
 * the theme is not using native WC hooks.
 */
class Theme_Uncode extends Theme_Bridge {
	/**
	 * Register the integration.
	 *
	 * @return void
	 */
	public function register() {
		$theme = wp_get_theme();

		if ( $theme->template !== 'uncode' ) {
			return;
		}

		add_action( 'woocommerce_before_variations_form', [ $this, 'display_discontinued_text' ] );
	}
}
