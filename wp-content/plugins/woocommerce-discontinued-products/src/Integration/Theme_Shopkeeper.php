<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Integration;

use Barn2\Plugin\WC_Discontinued_Products\Plugin;
use Barn2\WDP_Lib\Registerable;

/**
 * Provides integration with the Shopkeeper plugin.
 */
class Theme_Shopkeeper implements Registerable {
	/**
	 * Register the integration.
	 *
	 * @return void
	 */
	public function register() {
		$theme = wp_get_theme();
		if ( $theme->template !== 'shopkeeper' ) {
			return;
		}
		$this->init();
	}

	/**
	 * Hook into WP.
	 *
	 * @return void
	 */
	public function init() {
		add_filter( 'theme_mod_out_of_stock_label', [ $this, 'display_discontinued_text' ] );
	}

	/**
	 * Override the theme's custom label when the product is discontinued.
	 *
	 * @param string $label
	 * @return string
	 */
	public function display_discontinued_text( $label ) {
		global $product;

		if ( ! $product instanceof \WC_Product ) {
			return $label;
		}

		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID ) {
			return get_option( 'wcdp_text', esc_html__( 'Discontinued', 'woocommerce-discontinued-products' ) );
		}

		return $label;
	}

}
