<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Integration;

use Barn2\Plugin\WC_Discontinued_Products\Plugin;
use Barn2\Plugin\WC_Discontinued_Products\Util;
use Barn2\WDP_Lib\Registerable;

/**
 * Custom integration for the bridget theme because
 * the theme is not using native WC hooks.
 */
class Theme_Bridge implements Registerable {
	/**
	 * Register the integration.
	 *
	 * @return void
	 */
	public function register() {
		$theme = wp_get_theme();

		if ( $theme->template !== 'bridge' ) {
			return;
		}

		add_action( 'woocommerce_before_variations_form', [ $this, 'display_discontinued_text' ] );
	}

	public function display_discontinued_text() {
		global $product;

		if ( $product instanceof \WC_Product_Variable ) {
			$variations = Util::get_product_variations( $product );

			if ( empty( $variations ) ) {
				return $text;
			}

			$available_variations_count    = count( $variations );
			$discontinued_variations_count = 0;

			foreach ( $variations as $variation ) {
				if ( $variation instanceof \WC_Product_Variation && $variation->get_stock_status() === Plugin::DISCONTINUED_ID ) {
					$discontinued_variations_count++;
				}
			}

			if ( $available_variations_count === $discontinued_variations_count ) {
				$this->enqueue_inline_styling();
				echo '<p class="stock out-of-stock discontinued">' . get_option( 'wcdp_text', esc_html__( 'Discontinued', 'woocommerce-discontinued-products' ) ) . '</p>';
			}
		}
	}

	public function enqueue_inline_styling() {
		wp_register_style( 'bridge-dummy-handle', false );
		wp_enqueue_style( 'bridge-dummy-handle' );

		wp_add_inline_style( 'bridge-dummy-handle', 'p.stock.out-of-stock:not(.discontinued) { display: none !important; }' );
	}

}
