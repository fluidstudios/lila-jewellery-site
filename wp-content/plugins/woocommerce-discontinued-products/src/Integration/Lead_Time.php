<?php
/**
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Integration;

use Barn2\Plugin\WC_Discontinued_Products\Util;
use Barn2\Plugin\WC_Discontinued_Products\Plugin;
use Barn2\WDP_Lib\Registerable;

/**
 * Handles integration with the Lead Time extension.
 */
class Lead_Time implements Registerable {

	const SETTINGS_SECTION_ID = 'lead-time';

	/**
	 * Hook into WP
	 *
	 * @return void
	 */
	public function register() {

		if ( ! Util::is_lead_time_plugin_active() ) {
			return;
		}

		add_filter( 'woocommerce_get_settings_products', [ $this, 'get_settings' ], 15, 2 );
		add_filter( 'wclt_is_visible_stock_status', [ $this, 'register_stock_status' ] );
		add_filter( 'woocommerce_get_availability_text', [ $this, 'prevent_discontinued_display' ], 20, 2 );

	}

	/**
	 * Register the new discontinued status on the lead time settings panel.
	 *
	 * @param array $settings
	 * @param string $current_section
	 * @return array
	 */
	public function get_settings( $settings, $current_section ) {

		if ( $current_section !== self::SETTINGS_SECTION_ID ) {
			return $settings;
		}

		$out_of_stock_option = wp_list_filter( $settings, [ 'id' => 'wclt_display_backorder' ] );

		$discontinued_option = [
			'desc'          => __( 'Discontinued products', 'woocommerce-discontinued-products' ),
			'id'            => 'wclt_display_discontinued',
			'type'          => 'checkbox',
			'default'       => 'yes',
			'checkboxgroup' => '',
		];

		$settings = Util::array_insert( $settings, key( $out_of_stock_option ) + 1, $discontinued_option );

		return $settings;

	}

	/**
	 * Register the discontinued stock status within the Util::is_visible method
	 * of the lead time plugin.
	 *
	 * @param array $display list of stati
	 * @return array
	 */
	public function register_stock_status( $display ) {
		$display[ Plugin::DISCONTINUED_ID ] = get_option( 'wclt_display_discontinued', 'yes' );

		return $display;
	}

	/**
	 * Override the availability text once more when the product is both discontinued and has lead time.
	 *
	 * @param string $availability
	 * @param object $product WC product class
	 * @return string
	 */
	public function prevent_discontinued_display( $availability, $product ) {
		$lead_time = \Barn2\Plugin\WC_Lead_Time\Display::get_lead_time( $product );

		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID && $lead_time && get_option( 'wclt_display_discontinued', 'yes' ) ) {
			if ( apply_filters( 'wclt_disable_default_output', false ) ) {
				return $availability;
			}

			if ( ! \Barn2\Plugin\WC_Lead_Time\Util::is_visible( $product ) || empty( $availability ) || ! is_product() ) {
				return $availability;
			}

			$display_option = get_option( 'wclt_display_on_single_product', 'yes' );

			if ( $display_option !== 'yes' ) {
				return $availability;
			}

			$text_color   = get_option( 'wclt_text_color', '#60646c' );
			$availability = sprintf( '<span style="color: %s;" class="wclt_lead_time">%s</span>', $text_color, $lead_time );
		}

		return $availability;
	}
}
