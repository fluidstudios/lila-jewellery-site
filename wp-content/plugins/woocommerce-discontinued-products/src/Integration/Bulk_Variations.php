<?php
/**
 * @deprecated 1.0.1 WBV handles integration now.
 *
 * @package   Barn2\woocommerce-discontinued-products
 * @author    Barn2 Plugins <support@barn2.com>
 * @license   GPL-3.0
 * @copyright Barn2 Media Ltd
 */

namespace Barn2\Plugin\WC_Discontinued_Products\Integration;

use Barn2\Plugin\WC_Discontinued_Products\Util;
use Barn2\Plugin\WC_Discontinued_Products\Plugin;
use Barn2\WDP_Lib\Registerable;

/**
 * Handles integration with the bulk variations plugin.
 */
class Bulk_Variations implements Registerable {

	/**
	 * Hook into the extension and override the behaviour.
	 *
	 * @return void
	 */
	public function register() {
		if ( ! Util::is_bulk_variations_plugin_active() ) {
			return;
		}

		add_filter( 'wbv_variation_is_purchasable_in_grid', [ $this, 'display_discontinued_in_grid' ], 10, 2 );
		add_filter( 'wc_bulk_variations_qty_input_html', [ $this, 'display_status' ], 15, 3 );
	}

	/**
	 * Force the display of discontinued and non purchasable variations
	 *
	 * @param bool $visible
	 * @param object $variation WC product class
	 * @return bool
	 */
	public function display_discontinued_in_grid( $visible, $variation ) {

		if ( $variation->get_stock_status() === Plugin::DISCONTINUED_ID && ! $variation->is_purchasable() ) {
			return true;
		}

		return $visible;
	}

	/**
	 * Replace the quantity input with the stock html.
	 *
	 * @param string $input input field
	 * @param array $attrs attributes list
	 * @param object $product wc product object class
	 * @return string
	 */
	public function display_status( $input, $attrs, $product ) {

		if ( $product->get_stock_status() === Plugin::DISCONTINUED_ID && ! $product->is_purchasable() ) {
			$input = wc_get_stock_html( $product );
		}

		return $input;

	}
}
