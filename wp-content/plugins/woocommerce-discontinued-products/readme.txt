=== WooCommerce Discontinued Products ===
Contributors: andykeith, barn2media
Tags: woocommerce, discontinued, products
Requires at least: 4.7
Tested up to: 5.9
Requires PHP: 7.2
Stable tag: 1.0.5
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Provides an easy way to mark products as discontinued and choose whether to display them in your store.

== Description ==

Provides an easy way to mark products as discontinued and choose whether to display them in your store.

== Installation ==

1. Download the plugin from the Order Confirmation page or using the link in your order email.
1. Go to Plugins -> Add New -> Upload, and select the zip file you downloaded.
1. Once installed, click to Activate the plugin.
1. Go to WooCommerce -> Settings -> Products -> Discontinued Products, enter your license key and configure your settings.
1. See the [documentation](https://barn2.com/kb-categories/woocommerce-discontinued-products-kb/) for further details.

== Frequently Asked Questions ==

Please refer to [our support page](https://barn2.com/support-center/).

== Changelog ==

= 1.0.5 =
Release date 21 April 2022

 * Fix: discontinued status text not displaying for variable products with fully discontinued variations in the Bridge, Jupiterx and Uncode themes.
 * Dev: Tested up to WooCommerce 6.4.0.

= 1.0.4 =
Release date 08 March 2022

 * Fix: issue where other Barn2 plugins settings pages would appear distored in some cases.
 * Fix: improved the CSV import & export process that prevented the discontinued status to be assigned during import.
 * Dev: updated barn2 library.

= 1.0.3 =
Release date 23 February 2022

 * Fix: discontinued status would display twice inside the WooCommerce Quick View Pro modal.

= 1.0.2 =
Release date 17 February 2022

 * Fix: fixed an issue where the Shopkeeper theme would not display the discontinued status.
 * Tweak: changed the hooks used to mark products as discontinued.
 * Tweak: updated composer configuration to no longer check for platform requirements.
 * Tested up to WooCommerce 6.2

= 1.0.1 =
Release date 23 November 2021

 * Fix: integration with the Quick View Pro plugin.
 * Tweak: removed code no longer necessary.
 * Tweak: use wp_add_inline_script instead of wp_localize_script.
 * Dev: updated barn2-lib.
 * Tested up to WooCommerce 5.9

= 1.0 =
Release date 23 July 2021

* Initial release.
