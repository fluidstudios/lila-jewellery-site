<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'e517b6b397a211c4001ed7dc16496db0875854b9',
        'name' => 'barn2/woocommerce-discontinued-products',
        'dev' => false,
    ),
    'versions' => array(
        'barn2/woocommerce-discontinued-products' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'e517b6b397a211c4001ed7dc16496db0875854b9',
            'dev_requirement' => false,
        ),
    ),
);
