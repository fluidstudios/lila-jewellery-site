(function ($) {
	"use strict";

	/**
	 * Add a new option to the variations bulk actions dropdown
	 */
	 var wcdpAddDiscontinuedToVariations = function() {
		var opt = $('option[value=variable_stock_status_outofstock]');
		opt.after( '<option value="variable_stock_status_discontinued">' + wcdp_settings.discontinued_option + '</option>' );
	};

	$(document).ready(function () {
		wcdpAddDiscontinuedToVariations();
	});
})(jQuery);
