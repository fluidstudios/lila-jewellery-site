# Wordpress

# Installation

- Set language to UK (English).

# Plugins

- Delete base plugins.

## Install

- Advanced Custom Fields.
- Hummingbird – Speed Optimize, Cache, Minify & Defer Critical CSS/Javascript.
- Smush – Compress, Optimize and Lazy Load Images.
- SmartCrawl WordPress SEO checker, analyzer, and optimizer.
- WebP Express.
- WooCommerce (if required).
- Siteorigin Page builder.
- Duplicate post.

Activate all plugins.

Set up Smush.

Set up WebP Express:

- Settings.
- Save.
- `Conversion > Conversion Method > cwebp > Test`.
- Save settings & force `.htaccess`.

Set up Woocommerce (if required):

- Fill in address.
- Turn on PSPs.
- Uncheck 'Shipping Label' step.
- Set up shipping.
- Uncheck all extensions apart from WooCommerce Admin.
- Skip Jetpack step.
- Visit dashboard.
- WooCommerce > Settings > Payments > Untcheck Stripe & PayPal Checkout > Check Cheque].
- Pages > Delete Sample Page.

## Create All Pages

- Create all pages from sitemap.

## Settings

### General

- Empty tagline.

### Reading

- Set home page.
- Set posts page.
- Set feeds to summarise.

### Permalinks

- Set Post name to /sample-post.
- Set Product category base.
- Set Product category and Product base to category and tag.
- Save.
